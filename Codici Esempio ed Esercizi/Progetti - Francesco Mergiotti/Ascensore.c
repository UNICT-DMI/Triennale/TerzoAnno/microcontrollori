/*
 * File:   main.c
 * Author: Francesco Mergiotti
 *
 * Created on 28 febbraio 2017, 13.04
 */


#include <xc.h>
#include <stdio.h>
#include <stdlib.h>


#define TIMER_1_SECOND -62500 // 1 s
#define TIMER_500_MS -31250 // 500 ms
#define TIMER_250_MS -15625 // 250 ms  
#define TIMER_200_MS -12500 // 200 ms
#define ON 1
#define OFF 2
#define IDLE 0

#define FOR_PRESS 0
#define FOR_RELEASE 1

int timer = 0;
int timer_speed = 0;
int timer_standby = 0;
char standby = 0;
char door = 0;
char marcia = 0;
char counter = 0;
char piano_selezionato;
char piano_corrente = 0;

/*int (void)
{
    if (  == FOR_PRESS && PORTAbits.RA3 == 0)
    {
         = FOR_RELEASE;
        return 1;
    }
    else if ( == FOR_RELEASE && PORTAbits.RA3 == 1)
    {
         = FOR_PRESS;
    }
    return 0;
}*/

void setup_leds()
{
    // led ports as outputs
    TRISBbits.TRISB0 = 0;
    TRISBbits.TRISB1 = 0;
    TRISBbits.TRISB2 = 0;
    TRISBbits.TRISB3 = 0;
    TRISBbits.TRISB4 = 0;
    TRISBbits.TRISB5 = 0;
    
    // pushbutton pin as input
    TRISAbits.TRISA3 = 1;
    TRISAbits.TRISA2 = 1;
    TRISCbits.TRISC2 = 1;
}

void setup_timer(void)
{
    T0CONbits.TMR0ON = 0; // stop the timer
    T0CONbits.T08BIT = 0; // timer configured as 16-bit
    T0CONbits.T0CS = 0; // use system clock
    T0CONbits.PSA = 0; // use prescaler
    T0CONbits.T0PS = 0b111; // prescaler 1:256 ('0b' is a prefix for binary)    

    INTCONbits.T0IF = 0;
    INTCONbits.T0IE = 1;  // enable TMR0 interrupt generation
    
    TMR0 = TIMER_250_MS;
    
    T0CONbits.TMR0ON = 1; // start the timer    
}

void setup_adc(void)
{
    ANSELAbits.ANSA0 = 1; // RA0 = analog input --> AN0
    ANSELAbits.ANSA1 = 1; // RA1 = analog input --> AN1
    ADCON1bits.PVCFG = 0b00; // Positive reference = +VDD
    ADCON1bits.NVCFG = 0b00; // Negative reference = GND
    ADCON2bits.ADFM = 1; // format = right justified
    ADCON2bits.ACQT = 0b111; // acquisition time = 20*TAD
    ADCON2bits.ADCS = 0b110; // conversion clock = FOSC/64
    ADCON0bits.ADON = 1; // turn on ADC    
}

void setup_uart(void)
{
    // setup UART
    TRISCbits.TRISC6 = 0; // TX as output
    TRISCbits.TRISC7 = 1; // RX as input
    TXSTA1bits.SYNC = 0; // Async operation
    TXSTA1bits.TX9 = 0; // No tx of 9th bit
    TXSTA1bits.TXEN = 1; // Enable transmitter
    RCSTA1bits.RX9 = 0; // No rx of 9th bit
    RCSTA1bits.CREN = 1; // Enable receiver
    RCSTA1bits.SPEN = 1; // Enable serial port
    // Setting for 19200 BPS
    BAUDCON1bits.BRG16 = 0; // Divisor at 8 bit
    TXSTA1bits.BRGH = 0; // No high-speed baudrate
    SPBRG1 = 51; // divisor value for 19200
}

void putch(char c)
{
    while (TXSTA1bits.TRMT == 0) {}
    TXREG1 = c;
}

void regolazione_timer(void)
{
    // lettura AN0
    ADCON0bits.CHS = 0;
    ADCON0bits.GODONE = 1;
    while (ADCON0bits.GODONE == 1) {}

    timer_speed = 250 + ((2000 - 250) * ADRES / 1024);
    
    // lettura AN1
    ADCON0bits.CHS = 1;
    ADCON0bits.GODONE = 1;
    while (ADCON0bits.GODONE == 1) {}

    timer_standby = 3000 + ((3000 - 300) * ADRES / 1024);
    
    return;
}

void interrupt isr(void)
{
    if (INTCONbits.T0IF == 1)
    {
        INTCONbits.T0IF = 0;
        TMR0 = timer;
        //printf("standby %d\n\r", standby);
        if(standby == 1)
        {
            printf("entro qui e counter %d\n\r", counter);
            counter++;
            if(counter >= (timer_standby/250))
            {
                standby = 0;
                counter = 0;
            }
        }
        switch(door)
        {
            case 0:
                LATBbits.LATB5 = 1; //SPENGO IL LED - STANDBY
                break;
            case 1: // CLOSE DOOR
                counter++;
                if(counter < (1500/250))
                {
                    if(counter % 2 == 1)
                    {
                        LATBbits.LATB5 = 0;
                    }
                    else
                    {
                        LATBbits.LATB5 = 1;
                    }
                }
                else
                {
                    counter = 0;
                    door = 2;
                }
                break;
            case 2: // LED ACCESO FISSO
                LATBbits.LATB5 = 0; //ACCENDO IL LED
                break;
            case 3: //OPEN DOOR
                counter++;
                if(counter < (1500/200))
                {
                    if(counter % 2 == 1)
                    {
                        LATBbits.LATB5 = 0;
                    }
                    else
                    {
                        LATBbits.LATB5 = 1;
                    }
                }
                else
                {
                    counter = 0;
                    door = 0;
                }
                break;
        }
        if(marcia != 0)
        {
            counter++;
            if(counter >= (timer_speed/250))
            {
                counter = 0;
                if(marcia == 1) //UP
                    piano_corrente = (piano_corrente + 1) % 5;
                else if(marcia == 2) //DOWN
                    piano_corrente = (piano_corrente - 1) % 5;
                if(piano_corrente == piano_selezionato)
                {
                    counter = 0;
                    door = 3; // OPEN DOOR
                    marcia = 0;
                }
            }
        }
    }
}

void close_door(void)
{
    door = 1;
    timer = TIMER_250_MS;
    while(door == 1) {}
}

void open_door(void)
{
    door = 3;
    timer = TIMER_200_MS;
    while(door == 3) {}
}

void display_led(char piano_corrente)
{
    switch(piano_corrente)
    {
        case 0:
            LATBbits.LATB0 = 0;
            LATBbits.LATB1 = 1;
            LATBbits.LATB2 = 1;
            LATBbits.LATB3 = 1;    
            LATBbits.LATB4 = 1;
            break;
        case 1:
            LATBbits.LATB0 = 1;
            LATBbits.LATB1 = 0;
            LATBbits.LATB2 = 1;
            LATBbits.LATB3 = 1;    
            LATBbits.LATB4 = 1;
            break;
        case 2:  
            LATBbits.LATB0 = 1;
            LATBbits.LATB1 = 1;
            LATBbits.LATB2 = 0;
            LATBbits.LATB3 = 1;    
            LATBbits.LATB4 = 1;
            break;
        case 3: 
            LATBbits.LATB0 = 1;
            LATBbits.LATB1 = 1;
            LATBbits.LATB2 = 1;
            LATBbits.LATB3 = 0;    
            LATBbits.LATB4 = 1;
            break;
        case 4: 
            LATBbits.LATB0 = 1;
            LATBbits.LATB1 = 1;
            LATBbits.LATB2 = 1;
            LATBbits.LATB3 = 1;    
            LATBbits.LATB4 = 0;
            break;
    }
}

void stand_by()
{
    timer = TIMER_250_MS;
    standby = 1;
    while(standby == 1) {}
}

void up_down()
{
    timer = TIMER_250_MS;
    if(piano_selezionato > piano_corrente)
        marcia = 1;
    else if(piano_selezionato < piano_corrente)
        marcia = 2;
    else 
        marcia = 0;
    while(marcia != 0)
    {
        printf("\r");
        display_led(piano_corrente);
    }
}

void ascensore_on(char x)
{
    regolazione_timer();
    piano_selezionato = x;
    printf("Piano corrente: %d\n\r", piano_corrente);
    printf("Piano selezionato: %d\n\r", piano_selezionato);
    printf("Periodo di attesa : %d secondi\n\r", (timer_standby/1000));
    stand_by();
    printf("Chiudo Porte\n\r");
    close_door();
    printf("Ascensore in movimento\n\r");
    printf("Velocit� di movimento: %d piani/secondi\n\r", timer_speed);
    up_down();
    printf("Apro Porte\n\r");
    open_door();
    printf("Ascensore fermo\n\r");
    printf("Piano corrente: %d\n\r", piano_corrente);
}


void main(void)
{
    setup_uart();
    setup_adc();
    setup_timer();
    setup_leds();
    
    RCONbits.IPEN = 0;  // no priorities
    INTCONbits.GIE = 1; // global interrupt enable
    INTCONbits.PEIE = 1; // interrupt enable from peripherals
    
    LATBbits.LATB0 = 0; // IDLE STATE - PIANO ZERO
    
    for(;;)
    {
        if(PORTAbits.RA3 == 0) // PIANO 2
            if(piano_corrente != 4)
                ascensore_on(4);
        if(PORTAbits.RA2 == 0) // PIANO 1
            if(piano_corrente != 2)
                ascensore_on(2);
        if(PORTCbits.RC2 == 0) // PIANO 0
            if(piano_corrente != 0)
                ascensore_on(0);
    }
    return;
}