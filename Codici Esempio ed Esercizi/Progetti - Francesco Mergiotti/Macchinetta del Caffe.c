/*
 * File:   main.c
 * Author: Francesco Mergiotti
 *
 * Created on 26 ottobre 2016, 9.16
 */

#include <xc.h>

/* BOTTONI */
#define IDLE 0
#define CAFFE 1
#define LATTE 2
#define ZUCCHERO 3

/* Extra */
#define FOR_PRESS   0
#define FOR_RELEASE 1

int sugarBottonState = FOR_PRESS;
int coffeBottonState = FOR_PRESS;
int milkBottonState = FOR_PRESS;

/* TIMER*/
#define TIMERCAFFE 1000000
#define TIMERLATTE 1300000
#define TIMERZUCCHERO 50000

int sugarBottonPressed(void)
{
    if (sugarBottonState == FOR_PRESS && PORTCbits.RC2 == 0) {
      // we are checking ``PRESS'' and RA3 has been pressed
      sugarBottonState = FOR_RELEASE;
      return 1;
    }
    else if (sugarBottonState == FOR_RELEASE && PORTCbits.RC2 == 1) {
      // we are checking ``RELEASE'' and RA3 has been released
      sugarBottonState = FOR_PRESS;
    }
    return 0;
}

int coffeBottonPressed(void)
{
    if (coffeBottonState == FOR_PRESS && PORTAbits.RA2 == 0) {
      // we are checking ``PRESS'' and RA3 has been pressed
      coffeBottonState = FOR_RELEASE;
      return 1;
    }
    else if (coffeBottonState == FOR_RELEASE && PORTAbits.RA2 == 1) {
      // we are checking ``RELEASE'' and RA3 has been released
      coffeBottonState = FOR_PRESS;
    }
    return 0;
}

int milkBottonPressed(void)
{
    if (milkBottonState == FOR_PRESS && PORTAbits.RA3 == 0) {
      // we are checking ``PRESS'' and RA3 has been pressed
      milkBottonState = FOR_RELEASE;
      return 1;
    }
    else if (milkBottonState == FOR_RELEASE && PORTAbits.RA3 == 1) {
      // we are checking ``RELEASE'' and RA3 has been released
      milkBottonState = FOR_PRESS;
    }
    return 0;
}

void reset(int *zucchero)
{
    *zucchero = 0;
    LATBbits.LATB0 = 1;
    LATBbits.LATB1 = 1;
    LATBbits.LATB2 = 1;
    LATBbits.LATB3 = 1;
    LATBbits.LATB4 = 1;
    LATBbits.LATB5 = 1;
}


void event(int zucchero, int botton, long count)
{
    switch(botton)
    {
        case IDLE:
            break;
        case CAFFE:
            while(count < (TIMERZUCCHERO * zucchero))
            {
                LATBbits.LATB2 = 0;
                count++;
            }
            LATBbits.LATB2 = 1;
            count = 0;
            LATBbits.LATB0 = 0;
            while(count < TIMERCAFFE)
            {
                count++;
            }
            break;
        case LATTE:
            LATBbits.LATB2 = 0;
            while(count < (TIMERZUCCHERO * zucchero))
            {
                count++;
            }
            LATBbits.LATB2 = 1;
            count = 0;
            LATBbits.LATB1 = 0;
            while(count < TIMERLATTE)
            {
                count++;
            }
            LATBbits.LATB1 = 1;
            break;
        default:
            break;
    }
}

void controlloZucchero(int zucchero)
{
    switch(zucchero)
    {
        case 0:
            LATBbits.LATB3 = 1;
            LATBbits.LATB4 = 1;
            LATBbits.LATB5 = 1; 
            break;
        case 1:
            LATBbits.LATB3 = 0;
            break;
        case 2:
            LATBbits.LATB4 = 0;
            break;
        case 3:
            LATBbits.LATB5 = 0;
            break;
    }
}


void main(void)
{
    TRISAbits.TRISA3 = 1;   //Caff� semplice
    TRISAbits.TRISA2 = 1;   //Caffe Macchiato
    TRISCbits.TRISC2 = 1;   //Seleziono Zucchero
    TRISBbits.TRISB0 = 0;   //Erogazione Caffe
    TRISBbits.TRISB1 = 0;   //Erogazione Latte
    TRISBbits.TRISB2 = 0;   //Erogazione Zucchero
    TRISBbits.TRISB3 = 0;   //
    TRISBbits.TRISB4 = 0;   //  QNT Zucchero 1-2-3
    TRISBbits.TRISB5 = 0;   //
    
    int zucchero = 0;
    
    for(;;)
    {
        if (sugarBottonPressed())
        {
            zucchero++;
            zucchero = zucchero % 4;
            controlloZucchero(zucchero);
        }
        if(coffeBottonPressed())
        {
            event(zucchero, CAFFE, 0);
            reset(&zucchero);
        }
        if(milkBottonPressed())
        {
            event(zucchero, LATTE, 0);
            event(0, CAFFE, 0);
            reset(&zucchero);
        }
    }
    return;
}
