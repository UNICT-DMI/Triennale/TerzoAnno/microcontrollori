/*
 * File:   main.c
 * Author: Francesco Mergiotti
 *
 * Created on 3 marzo 2017, 11.50
 */

#include <xc.h>
#include <stdio.h>
#include <stdlib.h>


#define TIMER_50_MS -31 // 50 ms
#define TIMER_60_MS -37 // 60 ms
#define TIMER_10_MS -6 // 10 ms
#define TIMER_250_MS -15625 // 250 ms
#define ON 1
#define OFF 2
#define IDLE 0

#define FOR_PRESS 0
#define FOR_RELEASE 1

char dispositivo = 0;
unsigned int livello_carica = 100;
char pulsanteAccensione = FOR_PRESS;
char pulsanteCarica = FOR_PRESS;
char carica = 0;
char counter_accensione = 0;
char counter_carica = 0;

int pulsante_accensione(void)
{
    if ( pulsanteAccensione == FOR_PRESS && PORTAbits.RA3 == 0)
    {
        pulsanteAccensione = FOR_RELEASE;
        return 1;
    }
    else if ( pulsanteAccensione == FOR_RELEASE && PORTAbits.RA3 == 1)
    {
        pulsanteAccensione = FOR_PRESS;
    }
    return 0;
}

int pulsante_carica(void)
{
    if ( pulsanteCarica == FOR_PRESS && PORTAbits.RA3 == 0)
    {
        pulsanteCarica = FOR_RELEASE;
        return 1;
    }
    else if ( pulsanteCarica == FOR_RELEASE && PORTAbits.RA3 == 1)
    {
        pulsanteCarica = FOR_PRESS;
    }
    return 0;
}

void setup_digital_IO()
{
    // led ports as outputs
    TRISBbits.TRISB0 = 0;
    TRISBbits.TRISB1 = 0;
    TRISBbits.TRISB2 = 0;
    
    // pushbutton pin as input
    TRISAbits.TRISA3 = 1;
    TRISAbits.TRISA2 = 1;
}

void setup_timer(void)
{
    T0CONbits.TMR0ON = 0; // stop the timer
    T0CONbits.T08BIT = 0; // timer configured as 16-bit
    T0CONbits.T0CS = 0; // use system clock
    T0CONbits.PSA = 0; // use prescaler
    T0CONbits.T0PS = 0b111; // prescaler 1:256 ('0b' is a prefix for binary)    

    INTCONbits.T0IF = 0;
    INTCONbits.T0IE = 1;  // enable TMR0 interrupt generation
    
    TMR0 = TIMER_50_MS;
    
    T0CONbits.TMR0ON = 1; // start the timer    
}

void setup_adc(void)
{
    ANSELAbits.ANSA0 = 1; // RA0 = analog input --> AN0
    ADCON1bits.PVCFG = 0b00; // Positive reference = +VDD
    ADCON1bits.NVCFG = 0b00; // Negative reference = GND
    ADCON2bits.ADFM = 1; // format = right justified
    ADCON2bits.ACQT = 0b111; // acquisition time = 20*TAD
    ADCON2bits.ADCS = 0b110; // conversion clock = FOSC/64
    ADCON0bits.ADON = 1; // turn on ADC    
}

void setup_uart(void)
{
    // setup UART
    TRISCbits.TRISC6 = 0; // TX as output
    TRISCbits.TRISC7 = 1; // RX as input
    TXSTA1bits.SYNC = 0; // Async operation
    TXSTA1bits.TX9 = 0; // No tx of 9th bit
    TXSTA1bits.TXEN = 1; // Enable transmitter
    RCSTA1bits.RX9 = 0; // No rx of 9th bit
    RCSTA1bits.CREN = 1; // Enable receiver
    RCSTA1bits.SPEN = 1; // Enable serial port
    // Setting for 19200 BPS
    BAUDCON1bits.BRG16 = 0; // Divisor at 8 bit
    TXSTA1bits.BRGH = 0; // No high-speed baudrate
    SPBRG1 = 51; // divisor value for 19200
}

void putch(char c)
{
    while (TXSTA1bits.TRMT == 0) {}
    TXREG1 = c;
}

/*void regolazione_timeout(void)
{
    // lettura AN0
    ADCON0bits.CHS = 0;
    ADCON0bits.GODONE = 1;
    while (ADCON0bits.GODONE == 1) {}
    
    TP = ((20 * ADRES) / 1024);

    return;
}*/

void interrupt isr(void)
{
    if (INTCONbits.T0IF == 1)
    {
        INTCONbits.T0IF = 0;
        TMR0 = TIMER_50_MS;
        
        if(livello_carica > 5 && dispositivo)
        {
            if(counter_accensione < 5)
                counter_accensione++;
            else
            {
                counter_accensione = 0;
                livello_carica--;
            }
        }
        
        if(carica)
        {
            if(counter_carica < 6)
                counter_carica++;
            else
            {
                counter_carica = 0;
                livello_carica++;
            }
        }
    }
}

void accensione()
{
    dispositivo = !dispositivo;
    if(dispositivo)
    {
        printf("Dispositivo acceso\n\r");
        LATBbits.LATB4 = 0;
    }
    else
    {
        printf("Dispositivo spento\n\r");
        LATBbits.LATB4 = 1;
        carica = 0;
    }
}

void dispositivo_scarico()
{
    LATBbits.LATB0 = 1;
    LATBbits.LATB1 = 1;
    LATBbits.LATB2 = 1;
    LATBbits.LATB3 = 1;
    LATBbits.LATB4 = 1;
}

void controllo_carica()
{
    if(dispositivo)
    {
        if(100 >= livello_carica && livello_carica > 75)
        {
            LATBbits.LATB0 = 0;
            LATBbits.LATB1 = 0;
            LATBbits.LATB2 = 0;
            LATBbits.LATB3 = 0;
        }
        else if(75 > livello_carica && livello_carica > 50)
        {
            LATBbits.LATB0 = 1;
            LATBbits.LATB1 = 0;
            LATBbits.LATB2 = 0;
            LATBbits.LATB3 = 0;
        }
        else if(50 > livello_carica && livello_carica > 25)
        {
            LATBbits.LATB0 = 1;
            LATBbits.LATB1 = 1;
            LATBbits.LATB2 = 0;
            LATBbits.LATB3 = 0;
        }
        else if(25 > livello_carica && livello_carica > 5)
        {
            LATBbits.LATB0 = 1;
            LATBbits.LATB1 = 1;
            LATBbits.LATB2 = 1;
            LATBbits.LATB3 = 0;
        }
        else if(livello_carica == 5)
        {
            dispositivo_scarico();
        }
    }
    else
    {
        LATBbits.LATB0 = 1;
        LATBbits.LATB1 = 1;
        LATBbits.LATB2 = 1;
        LATBbits.LATB3 = 1;
    }
}

void main(void)
{
    setup_digital_IO();
    setup_adc();
    setup_timer();
    
    RCONbits.IPEN = 0;  // no priorities
    INTCONbits.GIE = 1; // global interrupt enable
    INTCONbits.PEIE = 1; // interrupt enable from peripherals
    
    for(;;)
    {
        controllo_carica();
        if(pulsante_accensione())
        {
            if(livello_carica > 5)
            {
                accensione();
            }
            else
                printf("Dispositivo scarico. Non posso accenderlo\n\r");
        }
        if(pulsante_carica())
        {
            if(dispositivo)
                carica = !carica;
        }
    }   
    return;
}
