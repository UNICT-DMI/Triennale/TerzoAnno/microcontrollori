/*
 * File:   main.c
 * Author: Francesco Mergiotti
 *
 * Created on 7 marzo 2017, 12.53
 */

#include <xc.h>
#include <stdio.h>
#include <stdlib.h>

#define TIME_250_MS  -15625 // 250 ms

unsigned int timer = 0;
char allarme_inserito = 0;
char allarme_attivo = 0;
char counter = 0;
char attivazione = 0;
char intrusione = 0;

#define FOR_PRESS 0
#define FOR_RELEASE 1

char statoBottoneInserimento= FOR_PRESS;
char statoBottoneDisattivazione = FOR_PRESS;
char statoBottoneSimulazione = FOR_PRESS;

int pushBottoneInserimento(void)
{
    if ( statoBottoneInserimento == FOR_PRESS && PORTAbits.RA3 == 0)
    {
        statoBottoneInserimento = FOR_RELEASE;
        return 1;
    }
    else if (statoBottoneInserimento == FOR_RELEASE && PORTAbits.RA3 == 1)
    {
        statoBottoneInserimento = FOR_PRESS;
    }
    return 0;
}

int pushBottoneDisattivazione(void)
{
    if ( statoBottoneDisattivazione == FOR_PRESS && PORTAbits.RA2 == 0)
    {
        statoBottoneDisattivazione = FOR_RELEASE;
        return 1;
    }
    else if (statoBottoneDisattivazione == FOR_RELEASE && PORTAbits.RA2 == 1)
    {
        statoBottoneDisattivazione = FOR_PRESS;
    }
    return 0;
}

int pushBottoneSimulazione(void)
{
    if ( statoBottoneSimulazione == FOR_PRESS && PORTCbits.RC2 == 0)
    {
        statoBottoneSimulazione = FOR_RELEASE;
        return 1;
    }
    else if (statoBottoneSimulazione == FOR_RELEASE && PORTCbits.RC2 == 1)
    {
        statoBottoneSimulazione = FOR_PRESS;
    }
    return 0;
}

void setup_leds()
{
    // led ports as outputs
    TRISBbits.TRISB0 = 0;
    TRISBbits.TRISB1 = 0;
    TRISBbits.TRISB2 = 0;
    TRISBbits.TRISB3 = 0;
    TRISBbits.TRISB4 = 0;
    TRISBbits.TRISB5 = 0;
    
    // pushbutton pin as input
    TRISAbits.TRISA3 = 1;
    TRISAbits.TRISA2 = 1;
    TRISCbits.TRISC2 = 1;
    
    LATBbits.LATB0 = 1;
    LATBbits.LATB1 = 1;
    LATBbits.LATB2 = 1;
    LATBbits.LATB3 = 1;
    LATBbits.LATB4 = 1;
    LATBbits.LATB5 = 1;
}

void setup_timer(void)
{
    T0CONbits.TMR0ON = 0; // stop the timer
    T0CONbits.T08BIT = 0; // timer configured as 16-bit
    T0CONbits.T0CS = 0; // use system clock
    T0CONbits.PSA = 0; // use prescaler
    T0CONbits.T0PS = 0b111; // prescaler 1:256 ('0b' is a prefix for binary)    

    INTCONbits.T0IF = 0;
    INTCONbits.T0IE = 1;  // enable TMR0 interrupt generation
    
    TMR0 = TIME_250_MS;
    
    T0CONbits.TMR0ON = 1; // start the timer    
}

void setup_adc(void)
{
    ANSELAbits.ANSA0 = 1; // RA0 = analog input --> AN0
    ANSELAbits.ANSA1 = 1; // RA1 = analog input --> AN1
    ADCON1bits.PVCFG = 0b00; // Positive reference = +VDD
    ADCON1bits.NVCFG = 0b00; // Negative reference = GND
    ADCON2bits.ADFM = 1; // format = right justified
    ADCON2bits.ACQT = 0b111; // acquisition time = 20*TAD
    ADCON2bits.ADCS = 0b110; // conversion clock = FOSC/64
    ADCON0bits.ADON = 1; // turn on ADC    
}

void setup_uart(void)
{
    // setup UART
    TRISCbits.TRISC6 = 0; // TX as output
    TRISCbits.TRISC7 = 1; // RX as input
    TXSTA1bits.SYNC = 0; // Async operation
    TXSTA1bits.TX9 = 0; // No tx of 9th bit
    TXSTA1bits.TXEN = 1; // Enable transmitter
    RCSTA1bits.RX9 = 0; // No rx of 9th bit
    RCSTA1bits.CREN = 1; // Enable receiver
    RCSTA1bits.SPEN = 1; // Enable serial port
    // Setting for 19200 BPS
    BAUDCON1bits.BRG16 = 0; // Divisor at 8 bit
    TXSTA1bits.BRGH = 0; // No high-speed baudrate
    SPBRG1 = 51; // divisor value for 19200
}

void putch(char c)
{
    while (TXSTA1bits.TRMT == 0) {}
    TXREG1 = c;
}

void interrupt isr(void)
{
    if (INTCONbits.T0IF == 1)
    {
        INTCONbits.T0IF = 0;
        TMR0 = TIME_250_MS;
        if(attivazione)
        {
            counter++;
            LATBbits.LATB1 = !LATBbits.LATB1;
            if(counter >= timer/250)
            {
                attivazione = 0;
                counter = 0;
                allarme_inserito = 1;
            }
        }
        
        if(intrusione)
        {
            counter++;
            if(counter >= timer/250)
            {
                intrusione = 0;
                LATBbits.LATB2 = 0;
                counter = 0;
            }
        }
    }
}

void calcola_tempo(void)
{
    // lettura AN0
    ADCON0bits.CHS = 0;
    ADCON0bits.GODONE = 1;
    while (ADCON0bits.GODONE == 1) {}

    timer = 2000 + ((6000 - 2000) * ADRES / 1023);
    
    return;
}

void inserisco_allarme()
{
    attivazione = 1;
    LATBbits.LATB0 = 0;
}

void attivo_simulazione()
{
    allarme_attivo = 1;
    intrusione = 1;
    LATBbits.LATB1 = 0;
}

void disattivo_allarme()
{
    allarme_attivo = 0;
    intrusione = 0;
    LATBbits.LATB1 = 1;
    LATBbits.LATB2 = 1;
}

void main(void)
{
    setup_uart();
    setup_adc();
    setup_timer();
    setup_leds();
    calcola_tempo();
    
    RCONbits.IPEN = 0;  // no priorities
    INTCONbits.GIE = 1; // global interrupt enable
    INTCONbits.PEIE = 1; // interrupt enable from peripherals
    
    for(;;)
    {
        
        if(pushBottoneInserimento())
            if(allarme_inserito == 0)
            {
                inserisco_allarme();
                calcola_tempo();
            }   
        if(pushBottoneSimulazione())
            if(allarme_attivo == 0)
            {
                attivo_simulazione();
                calcola_tempo();
            }
        if(pushBottoneDisattivazione())
            if(allarme_attivo)
            {
                calcola_tempo();
                disattivo_allarme();
            }
    }
    
    return;
}
