/*
 * File:   main.c
 * Author: Francesco Mergiotti
 *
 * Created on 7 marzo 2017, 15.53
 */


#include <xc.h>
#include <stdio.h>
#include <stdlib.h>

#define TIME_100_MS  -6250 // 100 ms

long TON = 0, TOFF = 0;
int timer = 0;
char counter = 0;
char acceso = 1;
int periodo = 0;

void setup_timer(void)
{
    T0CONbits.TMR0ON = 0; // stop the timer
    T0CONbits.T08BIT = 0; // timer configured as 16-bit
    T0CONbits.T0CS = 0; // use system clock
    T0CONbits.PSA = 0; // use prescaler
    T0CONbits.T0PS = 0b111; // prescaler 1:256 ('0b' is a prefix for binary)    

    INTCONbits.T0IF = 0;
    INTCONbits.T0IE = 1;  // enable TMR0 interrupt generation
    
    TMR0 = TIME_100_MS;
    
    T0CONbits.TMR0ON = 1; // start the timer    
}

void setup_adc(void)
{
    ANSELAbits.ANSA0 = 1; // RA0 = analog input --> AN0
    ANSELAbits.ANSA1 = 1; // RA1 = analog input --> AN1
    ADCON1bits.PVCFG = 0b00; // Positive reference = +VDD
    ADCON1bits.NVCFG = 0b00; // Negative reference = GND
    ADCON2bits.ADFM = 1; // format = right justified
    ADCON2bits.ACQT = 0b111; // acquisition time = 20*TAD
    ADCON2bits.ADCS = 0b110; // conversion clock = FOSC/64
    ADCON0bits.ADON = 1; // turn on ADC    
}

void setup_uart(void)
{
    // setup UART
    TRISCbits.TRISC6 = 0; // TX as output
    TRISCbits.TRISC7 = 1; // RX as input
    TXSTA1bits.SYNC = 0; // Async operation
    TXSTA1bits.TX9 = 0; // No tx of 9th bit
    TXSTA1bits.TXEN = 1; // Enable transmitter
    RCSTA1bits.RX9 = 0; // No rx of 9th bit
    RCSTA1bits.CREN = 1; // Enable receiver
    RCSTA1bits.SPEN = 1; // Enable serial port
    // Setting for 19200 BPS
    BAUDCON1bits.BRG16 = 0; // Divisor at 8 bit
    TXSTA1bits.BRGH = 0; // No high-speed baudrate
    SPBRG1 = 51; // divisor value for 19200
}

void putch(char c)
{
    while (TXSTA1bits.TRMT == 0) {}
    TXREG1 = c;
}

void interrupt isr(void)
{
    if (INTCONbits.T0IF == 1)
    {
        INTCONbits.T0IF = 0;
        TMR0 = TIME_100_MS;
        counter++;
        
        if(acceso)
        {
            if(counter >= (TON/100))
            {
                LATBbits.LATB0 = 1; // Turn off the LED
                counter = 0;
                acceso = 0;
            }
        }
        else
        {
            if(counter >= (TOFF/100))
            {
                LATBbits.LATB0 = 0; // Turn on the LED          
                counter = 0;
                acceso = 1;
            }
        }               
    }
}

void calcola_tempo(void)
{
    // lettura AN0
    ADCON0bits.CHS = 0;
    ADCON0bits.GODONE = 1;
    while (ADCON0bits.GODONE == 1) {}
   
    int adc_value = ADRES;
    printf("ADRES %d |", adc_value);
    periodo = 2 + (adc_value * 18) / 1023;
    periodo = periodo * 100;
    printf("periodo %d | ", periodo);
   
    // lettura AN1
    ADCON0bits.CHS = 1;
    ADCON0bits.GODONE = 1;
    while (ADCON0bits.GODONE == 1) {}
    
    TON = 10 * ADRES / 1023; // salvo le percentuali
    TON = TON * 10;
    TOFF = 100 - TON;         // salvo le percentuali
    printf("TON prima %d | ", TON);
    printf("TOFF prima %d | ", TOFF);
    
    TON = TON * periodo / 100;  // setto i tempi
    TOFF = TOFF * periodo / 100; // setto i tempi
    printf("TON dopo %d | ", TON);
    printf("TOFF dopo %d\n\r", TOFF);
    
    return;
}

void main(void)
{
    setup_timer();
    setup_adc();
    setup_uart();    
    TRISBbits.RB0 = 0;
    
    RCONbits.IPEN = 0;  // no priorities
    INTCONbits.GIE = 1; // global interrupt enable
    INTCONbits.PEIE = 1; // interrupt enable from peripherals

    calcola_tempo();
    
    for(;;)
    {
        calcola_tempo();
    }
    
    return;
}
