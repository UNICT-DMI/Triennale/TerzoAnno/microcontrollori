/* 
 * File:   tmr0_test_1.c
 * Author: corrado
 *
 * Created on October 18, 2015, 6:18 PM
 */

#include <xc.h>
#include <stdio.h>
#include <stdlib.h>

#define TON  31250 //500 ms
#define TOFF 62500 //1 sec

int main(int argc, char** argv) {
    TRISBbits.TRISB0 = 0; // output
    TRISBbits.TRISB1 = 0; // led sempre accesso
    
    T0CONbits.TMR0ON = 0; // stop the timer
    T0CONbits.T08BIT = 0; // timer configured as 16-bit
    T0CONbits.T0CS = 0; // use system clock
    T0CONbits.PSA = 0; // use prescaler
    T0CONbits.T0PS = 0b011; // prescaler 1:16 ('0b' is a prefix for binary)
    TMR0 = -TON; // clear timer value
    INTCONbits.T0IF = 0;
    T0CONbits.TMR0ON = 1; // start the timer
    
    LATBbits.LATB0 = 0; // Turn on initially the LED
    LATBbits.LATB1 = 0;
    for (;;) {
        while (INTCONbits.T0IF != 1) {} 
        TMR0 = -TOFF;
        INTCONbits.T0IF = 0;
        LATBbits.LATB0 = 1; // Turn off the LED
                
        while (INTCONbits.T0IF != 1) {} 
        TMR0 = -TON;
        INTCONbits.T0IF = 0;
        LATBbits.LATB0 = 0; // Turn on the LED                
    }

    return (EXIT_SUCCESS);
}

