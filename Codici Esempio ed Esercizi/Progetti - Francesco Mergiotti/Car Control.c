/*
 * File:   main.c
 * Author: Francesco Mergiotti
 *
 * Created on 27 febbraio 2017, 12.00
 */

#include <xc.h>
#include <stdio.h>
#include <stdlib.h>

#define TIME_1_SECOND -62500 // 1 s
#define TON -31250 // 500 ms  
#define Toff_continuo 1 // 1 s
#define PERIOD_LOW 1 // 1 s
#define PERIOD_HIGH 5 // 5 sec

#define ON 1
#define OFF 2
#define IDLE 0

#define FOR_PRESS 0
#define FOR_RELEASE 1

char statoBottoneTergicristalloContinuo = FOR_PRESS;
char statoBottoneTergicristalloTemporizzato = FOR_PRESS;

char Toff_temporizzato = 0;
char counter = 0;
char statoTergicristallo = 0; // 0 Spento - 1 Continuio - 2 Temporizzato
char stato = OFF; // Stato led del tergicristallo - Acceso/Spento

int pushButtonContinuo(void)
{
    if ( statoBottoneTergicristalloContinuo == FOR_PRESS && PORTAbits.RA3 == 0)
    {
        statoBottoneTergicristalloContinuo = FOR_RELEASE;
        return 1;
    }
    else if (statoBottoneTergicristalloContinuo == FOR_RELEASE && PORTAbits.RA3 == 1)
    {
        statoBottoneTergicristalloContinuo = FOR_PRESS;
    }
    return 0;
}
int pushButtonTemporizzato()
{
    if ( statoBottoneTergicristalloTemporizzato == FOR_PRESS && PORTAbits.RA2 == 0)
    {
        statoBottoneTergicristalloTemporizzato = FOR_RELEASE;
        return 1;
    }
    else if (statoBottoneTergicristalloTemporizzato == FOR_RELEASE && PORTAbits.RA2 == 1)
    {
        statoBottoneTergicristalloTemporizzato = FOR_PRESS;   
    }
    return 0;
}

void setup_leds()
{
    // led ports as outputs
    TRISBbits.TRISB0 = 0;
    TRISBbits.TRISB1 = 0;
    TRISBbits.TRISB2 = 0;
    TRISBbits.TRISB3 = 0;
    TRISBbits.TRISB4 = 0;
    TRISBbits.TRISB5 = 0;
    
    // pushbutton pin as input
    TRISAbits.TRISA3 = 1;
    TRISAbits.TRISA2 = 1;
    TRISCbits.TRISC2 = 1;
    
    LATBbits.LATB0 = 1;
    LATBbits.LATB1 = 1;
    LATBbits.LATB2 = 1;
    LATBbits.LATB3 = 1;
    LATBbits.LATB4 = 1;
    LATBbits.LATB5 = 1;
}

void setup_timer(void)
{
    T0CONbits.TMR0ON = 0; // stop the timer
    T0CONbits.T08BIT = 0; // timer configured as 16-bit
    T0CONbits.T0CS = 0; // use system clock
    T0CONbits.PSA = 0; // use prescaler
    T0CONbits.T0PS = 0b111; // prescaler 1:256 ('0b' is a prefix for binary)    

    INTCONbits.T0IF = 0;
    INTCONbits.T0IE = 1;  // enable TMR0 interrupt generation
    
    TMR0 = TON;
    
    T0CONbits.TMR0ON = 1; // start the timer    
}

void setup_adc(void)
{
    ANSELAbits.ANSA0 = 1; // RA0 = analog input --> AN0
    ANSELAbits.ANSA1 = 1; // RA1 = analog input --> AN1
    ADCON1bits.PVCFG = 0b00; // Positive reference = +VDD
    ADCON1bits.NVCFG = 0b00; // Negative reference = GND
    ADCON2bits.ADFM = 1; // format = right justified
    ADCON2bits.ACQT = 0b111; // acquisition time = 20*TAD
    ADCON2bits.ADCS = 0b110; // conversion clock = FOSC/64
    ADCON0bits.ADON = 1; // turn on ADC    
}

void setup_uart(void)
{
    // setup UART
    TRISCbits.TRISC6 = 0; // TX as output
    TRISCbits.TRISC7 = 1; // RX as input
    TXSTA1bits.SYNC = 0; // Async operation
    TXSTA1bits.TX9 = 0; // No tx of 9th bit
    TXSTA1bits.TXEN = 1; // Enable transmitter
    RCSTA1bits.RX9 = 0; // No rx of 9th bit
    RCSTA1bits.CREN = 1; // Enable receiver
    RCSTA1bits.SPEN = 1; // Enable serial port
    // Setting for 19200 BPS
    BAUDCON1bits.BRG16 = 0; // Divisor at 8 bit
    TXSTA1bits.BRGH = 0; // No high-speed baudrate
    SPBRG1 = 51; // divisor value for 19200
}

void putch(char c)
{
    while (TXSTA1bits.TRMT == 0) {}
    TXREG1 = c;
}

void interrupt isr(void)
{
    if (INTCONbits.T0IF == 1)
    {
        switch(statoTergicristallo)
        {
            case 1: //CONTINUO
                if(stato == ON)
                {
                    LATBbits.LATB0 = 1; //Spengo il LED
                    TMR0 = TIME_1_SECOND;
                    INTCONbits.T0IF = 0;
                    stato = OFF;
                }
                else
                {
                    LATBbits.LATB0 = 0; //Accendo il LED
                    TMR0 = TON;
                    INTCONbits.T0IF = 0;
                    stato = ON;
                }
                break;
            case 2: // TEMPORIZZATO
                if(stato == ON)
                {
                    LATBbits.LATB0 = 1; //Spengo il LED
                    TMR0 = TIME_1_SECOND;
                    INTCONbits.T0IF = 0;
                    stato = OFF;
                }
                else
                {
                    counter++;
                    TMR0 = TIME_1_SECOND;
                    INTCONbits.T0IF = 0;
                    if(counter > Toff_temporizzato)
                    {
                        LATBbits.LATB0 = 0; //Accendo il LED
                        TMR0 = TON;
                        INTCONbits.T0IF = 0;
                        stato = ON;
                        counter = 0;
                    }
                }
                break;
            default:
                INTCONbits.T0IF = 0;
                TMR0 = TON;
                break;
        }
    }
}

void tergicristallo_temporizzato(void)
{
    // lettura AN0
    ADCON0bits.CHS = 0;
    ADCON0bits.GODONE = 1;
    while (ADCON0bits.GODONE == 1) {}

    Toff_temporizzato = PERIOD_LOW + ((PERIOD_HIGH - PERIOD_LOW) * ADRES / 1024);
    printf("TOFF Temporizzato: %d\r", Toff_temporizzato);
    // lettura AN1
    /*ADCON0bits.CHS = 1;
    ADCON0bits.GODONE = 1;
    while (ADCON0bits.GODONE == 1) {}

    TS = (15*ADRES) / 1023; */
    return;
}

void main(void)
{
    setup_uart();
    setup_adc();
    setup_timer();
    setup_leds();
    
    RCONbits.IPEN = 0;  // no priorities
    INTCONbits.GIE = 1; // global interrupt enable
    INTCONbits.PEIE = 1; // interrupt enable from peripherals
    
    for(;;)
    {
        if(pushButtonContinuo())
        {
            if(statoTergicristallo == 0 || statoTergicristallo == 2)
                statoTergicristallo = 1;
            else
                statoTergicristallo = 0;
        }
        if(pushButtonTemporizzato())
        {
            if(statoTergicristallo == 0 || statoTergicristallo == 1)
                statoTergicristallo = 2;
            else
                statoTergicristallo = 0;
            tergicristallo_temporizzato();
        }
    }
    return;
}