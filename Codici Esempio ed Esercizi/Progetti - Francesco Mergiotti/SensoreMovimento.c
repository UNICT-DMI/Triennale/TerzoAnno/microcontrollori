/*
 * File:   main.c
 * Author: Francesco Mergiotti
 *
 * Created on 2 marzo 2017, 12.56
 */


#include <xc.h>
#include <stdio.h>
#include <stdlib.h>


#define TIMER_500_MS -31250 // 500 ms
#define ON 1
#define OFF 2
#define IDLE 0

#define FOR_PRESS 0
#define FOR_RELEASE 1

char TP = 0;
char counter = 0;
char energy_save = 0;
char progressione = 0;
char bottoneAttivazione = FOR_PRESS;
char bottoneMovimento = FOR_PRESS;

int attivazione(void)
{
    if ( bottoneAttivazione == FOR_PRESS && PORTAbits.RA3 == 0)
    {
        bottoneAttivazione = FOR_RELEASE;
        return 1;
    }
    else if ( bottoneAttivazione == FOR_RELEASE && PORTAbits.RA3 == 1)
    {
        bottoneAttivazione = FOR_PRESS;
    }
    return 0;
}

int movimento(void)
{
    if ( bottoneMovimento == FOR_PRESS && PORTAbits.RA2 == 0)
    {
        bottoneMovimento = FOR_RELEASE;
        return 1;
    }
    else if ( bottoneMovimento == FOR_RELEASE && PORTAbits.RA2 == 1)
    {
        bottoneMovimento = FOR_PRESS;
    }
    return 0;
}

void setup_digital_IO()
{
    // led ports as outputs
    TRISBbits.TRISB0 = 0;
    TRISBbits.TRISB1 = 0;
    TRISBbits.TRISB2 = 0;
    
    // pushbutton pin as input
    TRISAbits.TRISA3 = 1;
    TRISAbits.TRISA2 = 1;
}

void setup_timer(void)
{
    T0CONbits.TMR0ON = 0; // stop the timer
    T0CONbits.T08BIT = 0; // timer configured as 16-bit
    T0CONbits.T0CS = 0; // use system clock
    T0CONbits.PSA = 0; // use prescaler
    T0CONbits.T0PS = 0b111; // prescaler 1:256 ('0b' is a prefix for binary)    

    INTCONbits.T0IF = 0;
    INTCONbits.T0IE = 1;  // enable TMR0 interrupt generation
    
    TMR0 = TIMER_500_MS;
    
    T0CONbits.TMR0ON = 1; // start the timer    
}

void setup_adc(void)
{
    ANSELAbits.ANSA0 = 1; // RA0 = analog input --> AN0
    ADCON1bits.PVCFG = 0b00; // Positive reference = +VDD
    ADCON1bits.NVCFG = 0b00; // Negative reference = GND
    ADCON2bits.ADFM = 1; // format = right justified
    ADCON2bits.ACQT = 0b111; // acquisition time = 20*TAD
    ADCON2bits.ADCS = 0b110; // conversion clock = FOSC/64
    ADCON0bits.ADON = 1; // turn on ADC    
}

void setup_uart(void)
{
    // setup UART
    TRISCbits.TRISC6 = 0; // TX as output
    TRISCbits.TRISC7 = 1; // RX as input
    TXSTA1bits.SYNC = 0; // Async operation
    TXSTA1bits.TX9 = 0; // No tx of 9th bit
    TXSTA1bits.TXEN = 1; // Enable transmitter
    RCSTA1bits.RX9 = 0; // No rx of 9th bit
    RCSTA1bits.CREN = 1; // Enable receiver
    RCSTA1bits.SPEN = 1; // Enable serial port
    // Setting for 19200 BPS
    BAUDCON1bits.BRG16 = 0; // Divisor at 8 bit
    TXSTA1bits.BRGH = 0; // No high-speed baudrate
    SPBRG1 = 51; // divisor value for 19200
}

void putch(char c)
{
    while (TXSTA1bits.TRMT == 0) {}
    TXREG1 = c;
}

void regolazione_timeout(void)
{
    // lettura AN0
    ADCON0bits.CHS = 0;
    ADCON0bits.GODONE = 1;
    while (ADCON0bits.GODONE == 1) {}
    
    TP = ((20 * ADRES) / 1024);

    return;
}

void interrupt isr(void)
{
    if (INTCONbits.T0IF == 1)
    {
        INTCONbits.T0IF = 0;
        TMR0 = TIMER_500_MS;
        
        if(progressione == 1)
        {
            LATBbits.LATB1 = !LATBbits.LATB1;
            counter++;
            printf("counter e TP: %d - %d\n\r", counter, TP);
            if(counter >= TP)
            {
                progressione = 0;
                energy_save = 1;
            }
        }
    }
}

void go_to_energy_save()
{
    LATBbits.LATB2 = 0;
    LATBbits.LATB0 = 0;
    LATBbits.LATB1 = 1;
}

void main(void)
{
    setup_digital_IO();
    setup_adc();
    setup_timer();
    setup_uart();
    
    RCONbits.IPEN = 0;  // no priorities
    INTCONbits.GIE = 1; // global interrupt enable
    INTCONbits.PEIE = 1; // interrupt enable from peripherals
    
    regolazione_timeout();
    for(;;)
    {    
        regolazione_timeout();
        if(attivazione())
        {   
            if(progressione && energy_save)
                progressione = 0;
            else if(progressione && energy_save == 0)
                progressione = 0;
            else if(progressione == 0 && energy_save)
                progressione = 0;
            else if(progressione == 0 && energy_save == 0)
                progressione = 1;
            
            energy_save = 0;
            counter = 0;
            
            LATBbits.LATB0 = !LATBbits.LATB0;
            LATBbits.LATB2 = 1;
            LATBbits.LATB1 = 1;
        }
        if(energy_save == 1)
        {
            go_to_energy_save();
        }
        if(movimento())
        {
            if(progressione || energy_save)
            {
                counter = 0;
                progressione = 1;
                if(energy_save)
                {
                    energy_save = 0;
                    LATBbits.LATB2 = 1;
                }
            }
        }
    }
    
    return;
}