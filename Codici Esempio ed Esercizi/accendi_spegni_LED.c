/* Coded by Helias */

#include <xc.h>

#define CHECK_FOR_PRESS 0
#define CHECK_FOR_RELEASE 1

void main(void) {

	int state = CHECK_FOR_PRESS;
    TRISAbits.TRISA3 = 1; /* button */
	TRISBbits.TRISB0 = 0; /* LED */

	for (;;) {
        if (state == CHECK_FOR_PRESS && PORTAbits.RA3 == 0) {
			LATBbits.LATB0 = !LATBbits.LATB0;
			state = CHECK_FOR_RELEASE;
        }
		else if (state == CHECK_FOR_RELEASE && PORTAbits.RA3 == 1)
			state = CHECK_FOR_PRESS;
	}

}
