/*
 * calculator.c
 */

#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>

int calculator(char * s, float * result)
{
    float first_operand, second_operand;
    char operation;
    char *operand_start, *operand_end;

    char tmp[30];
    int i;

    // ---------------------------------- FIRST OPERAND ----------------------------------

    // first skip spaces
    while (*s == ' ') ++s;

    if (*s == 0) return 1; // string premature ended, so return a syntax error

    // now see if the first operand is starting
    if (!isdigit(*s) && *s != '.' ) return 1; // no valid numeric symbol found, syntax error

    // start of first operand has been identified!!
    operand_start = s;
    ++s;

    // now wait for the end of the operand
    while (isdigit(*s) || *s == '.' ) ++s;

    if (*s == 0) return 1; // string premature ended, so return a syntax error

    // end of first operand got!!!
    operand_end = s;

    // now copy from operand_start to operand_end into a temporary string
    // and convert it into float
    i = 0;
    while (operand_start != operand_end) {
        tmp[i] = *operand_start;
        ++operand_start;
        ++i;
    }
    // add end of string character
    tmp[i] = 0;
    // now convert it to float
    first_operand = atof(tmp);

    // ---------------------------------- OPERATION ----------------------------------

    // now we should search the operation symbol, but first there could be same spaces
    while (*s == ' ') ++s;

    if (*s == 0) return 1; // string premature ended, so return a syntax error

    // the operation is here
    operation = *s;

    if (operation != '+' && operation != '-' && operation != '*' && operation != '/' ) {
        // an invalid symbol has been found, syntax error!
        return 1;
    }
    ++s;

    // ---------------------------------- SECOND OPERAND ----------------------------------

    // first skip spaces
    while (*s == ' ') ++s;

    if (*s == 0) return 1; // string premature ended, so return a syntax error

    // now see if the second operand is starting
    if (!isdigit(*s) && *s != '.' ) return 1; // no valid numeric symbol found, syntax error

    // start of first operand identified!!
    operand_start = s;
    ++s;

    // now wait for the end of the operand
    while (isdigit(*s) || *s == '.' ) ++s;

    // end of first operand got!!!
    operand_end = s;

    // ok at the end of the string, we need only spaces and end of string
    // other characters results in a syntax error
    while (*s == ' ') s++;
    if (*s != 0) return 1; //


    // now copy from operand_start to operand_end into a temporary string
    // and convert it into float
    i = 0;
    while (operand_start != operand_end) {
        tmp[i] = *operand_start;
        ++operand_start;
        ++i;
    }
    // add end of string character
    tmp[i] = 0;
    // now convert it to float
    second_operand = atof(tmp);

    // everything OK, do the operation

    // do the operation
    switch (operation) {
    case '+': *result = first_operand + second_operand; break;
    case '-': *result = first_operand - second_operand; break;
    case '*': *result = first_operand * second_operand; break;
    case '/': *result = first_operand / second_operand; break;
    }

    return 0;

}


void main()
{
    float result;
    char * s;

    s = "1.34 +   6.2";
    printf("Computing '%s' = ", s);
    if (calculator(s, &result) == 1)
        printf("SYNTAX ERROR\n");
    else
        printf("%f\n", result);

    s = "   1.34+6.2   ";
    printf("Computing '%s' = ", s);
    if (calculator(s, &result) == 1)
        printf("SYNTAX ERROR\n");
    else
        printf("%f\n", result);

    s = "1   1.34+6.2   ";
    printf("Computing '%s' = ", s);
    if (calculator(s, &result) == 1)
        printf("SYNTAX ERROR\n");
    else
        printf("%f\n", result);

    s = "10*5";
    printf("Computing '%s' = ", s);
    if (calculator(s, &result) == 1)
        printf("SYNTAX ERROR\n");
    else
        printf("%f\n", result);


}

