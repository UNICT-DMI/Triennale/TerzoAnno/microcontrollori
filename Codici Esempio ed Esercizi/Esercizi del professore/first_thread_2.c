#include <stdio.h>
#include <pthread.h>

int global;

struct params {
  int one, two, three;
};

void * proc1(void * x)
{
   int i;
   struct params * p;
   p = (struct params *)x;
   printf("Parametri %d, %d, %d\n", p->one, p->two, p->three);
   for (i = 0;i < 10;i++) {
      printf("PROC1: %d, %d\n", i, global);
      sleep(1);
   }
   return NULL;
}


int main(int argc, char ** argv)
{
   int i;
   pthread_t first_thread;
   struct params parametri;

   parametri.one = 1;
   parametri.two = 2;
   parametri.three = 3;
   pthread_create(&first_thread, NULL, proc1, &parametri);
   for (i = 0;i < 5;i++) {
      printf("MAIN: %d\n", i);
      global = i * 3;
      sleep(1);
   }
   pthread_join(first_thread, NULL);
   return 0;
}

