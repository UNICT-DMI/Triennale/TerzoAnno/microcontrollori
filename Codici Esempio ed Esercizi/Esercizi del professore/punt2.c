#include <stdio.h>

void incr(int * p)
{
  *p = *p + 1;
}

void main()
{
    int i;
    i = 10;
    printf("%d\n", i);
    incr(&i);
    printf("%d\n", i);
}

