
#include <stdio.h>
#include <stdlib.h>

const int data_size = 10;

int calc_max(int * data, int size)
{
    printf("Calculating max....\n");

    int m = data[0];

    for (int i = 0; i < size;i++) {
        if (data[i] > m)
            m = data[i];
    }
    return m;
}

void calc_max_and_index(int * data, int size,
                        int & max_value, int & max_index)
{
    max_value = data[0];
    max_index = 0;

    for (int i = 0; i < size;i++) {
        if (data[i] > max_value) {
            max_value = data[i];
            max_index = i;
        }
    }
}


main()
{
    int * array = (int *)malloc(sizeof(int) * data_size);

    for (int i = 0; i < data_size;i++)
        array[i] = rand() % 50;

    printf("Max = %d\n", calc_max(array, data_size));


    int m, i;
    calc_max_and_index(array, data_size, m, i);
    printf("Max = %d, Max index = %d\n", m, i);

}
/*
void calc_max_and_index(int * data, int size,
                        int & max_value, int & max_index)
*/


