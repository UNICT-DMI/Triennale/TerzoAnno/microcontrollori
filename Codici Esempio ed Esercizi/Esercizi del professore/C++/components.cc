/*
 * components.cc
 */

#include <iostream>
#include <string>

using namespace std;

template <class T>
class Component {
    string type, name, measure_unit;
    T value;
public:
    Component(string _ty, string _na, T _val, string _measure)
    {
        type = _ty; name = _na; value = _val; measure_unit = _measure;
    };
    T get_value() { return value;};
    string & get_type() { return type; };
    string & get_name() { return name; };
    string & get_measure_unit() { return measure_unit; };
};


class Resistor : public Component<float> {
public:
    Resistor(string _name, float _val) : Component<float>("Resistor", _name, _val, "Ohm") {} ;
};


class Capacitor : public Component<float> {
public:
    Capacitor(string _name, float _val) : Component<float>("Capacitor", _name, _val, "Farad") {} ;
};


class Transistor : public Component<string> {
public:
    Transistor(string _name, string _val) : Component<string>("Transistor", _name, _val, "") {} ;
};


template <class T>
ostream & operator<<(ostream & out, Component<T> & c)
{
    out << c.get_name() << "(" << c.get_type() << ") :" << c.get_value() << " " << c.get_measure_unit();
    return out;
}

main()
{
    Resistor r("R1", 1500);
    Capacitor c("C1", 0.000001);
    Transistor tr("T1", "2N2222");

    cout << r << endl;
    cout << c << endl;
    cout << tr << endl;
}

