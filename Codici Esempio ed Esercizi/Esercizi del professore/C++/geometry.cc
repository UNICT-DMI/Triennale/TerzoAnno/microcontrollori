/*
 * geometry.cc
 */

#include <iostream>


class Polygon {
    int n_sides;
    float * sides;
public:
    Polygon(int sd);
    ~Polygon();
    void set_side(int side_num, float side_val)
    {
        sides[side_num] = side_val;
    };
    float get_side(int side_num)
    {
        return sides[side_num];
    };
    virtual float perimeter();
    virtual float area() = 0;
};

class Rectangle : public Polygon {
public:
    Rectangle(float h, float w);
    virtual float area();
};


class Square : public Rectangle {
public:
    Square(float s);
};


Polygon::Polygon(int sd)
{
    std::cout << "Creating the polygon..." << std::endl;
    this->n_sides = sd;
    this->sides = new float[sd];
}

Polygon::~Polygon()
{
    std::cout <<  "Destroying the polygon..." << std::endl;
    delete [] sides;
}

float Polygon::perimeter()
{
    float p = 0;
    for (int i = 0; i < n_sides;i++)
        p += sides[i];
    return p;
}

Rectangle::Rectangle(float h, float w) : Polygon(4)
{
    set_side(0, h);
    set_side(1, w);
    set_side(2, h);
    set_side(3, w);
}

float Rectangle::area()
{
    return get_side(0) * get_side(1);
}

Square::Square(float s) : Rectangle(s, s)
{
}


void show_perimeter(const char * poly_name, Polygon * p)
{
    std::cout << "The perimeter of the" << poly_name << " is " << p->perimeter() << std::endl;
}

void show_area(const char * poly_name, Polygon * p)
{
    std::cout << "The area of the " << poly_name << " is " << p->area() << std::endl;
}

main()
{
    Rectangle * r = new Rectangle(10, 20);
    Square * s = new Square(30);

    show_perimeter("Rectangle", r);
    show_perimeter("Square", s);

    show_area("Rectangle", r);
    show_area("Square", s);

    delete r;
    delete s;
}


