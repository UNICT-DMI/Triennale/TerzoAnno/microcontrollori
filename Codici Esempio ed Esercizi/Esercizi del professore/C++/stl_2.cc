/*
 * stl_2.cc
 */

#include <iostream>
#include <map>

using namespace std;


main()
{
    map<string, float> dict;

    cout << "Insert data" << endl;
    for (;;) {
        string key;
        float data;
        cout << "Enter key ('q' quit) ";
        cin >> key;
        if (key == "q") break;
        cout << "Enter data ";
        cin >> data;
        dict[key] = data;
    }

    cout << "-----------------------------------------" << endl;
    for (map<string,float>::iterator it = dict.begin(); it != dict.end(); it++)
        cout << "Key, Value pair are " << it->first << ", " << it->second << endl;

    cout << "-----------------------------------------" << endl;

    cout << "Search for data"  << endl;
    for (;;) {
        string key;
        float data;
        cout << "Enter key ";
        cin >> key;
        if (dict.find(key) == dict.end())
            cout << "Key is not present!" << endl;
        else
            cout << "Data is " << dict[key] << endl;
    cout << "-----------------------------------------" << endl;
    }
}
