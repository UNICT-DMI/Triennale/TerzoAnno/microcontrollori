/*
 * logger.cc
 */

#include <iostream>
#include <fstream>
#include <string>

using namespace std;

class Logger {
    ofstream log_file;
public:
    Logger(const char * filename = "default.log");
    ~Logger();
    void log(char * s);
    void log(int i);
    void log(double f);
    void log(string s);
};


Logger::Logger(const char * filename)
{
    log_file.open(filename);
}

Logger::~Logger()
{
    log_file.close();
}

void Logger::log(char * s)
{
    log_file << "LOG_STR: " << s << "\n";
}

void Logger::log(int i)
{
    log_file << "LOG_INT: " << i << "\n";
}

void Logger::log(double f)
{
    log_file << "LOG_DOUBLE: " << f << "\n";
}

void Logger::log(string s)
{
    log_file << "LOG_STR: " << s << "\n";
}



main()
{
    string fname, log_str;

    cout << "Inserisci il nome del file di log:";
    cin >> fname;

    Logger * my_logger = new Logger(fname.c_str());

    for(;;) {
        cout << "Inserisci la stringa di log ('q' per terminare):";
        cin >> log_str;
        //if (strcmp(log_str.c_str(),"q") == 0) break;
        if (log_str == "q")
            break;
        my_logger->log(log_str);
    }

    delete my_logger;
}


