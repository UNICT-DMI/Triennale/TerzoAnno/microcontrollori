/*
 * first_class.cc
 */

#include <stdio.h>

class Animal {
    int age;
    char * name;
public:
    Animal(int a, char * n);
    ~Animal();
    void show(void);
};

/* object constructor */
Animal::Animal(int a, char * n)
{
    age = a;
    name = n;
    printf("Animal %s is being created\n", name);
}

/* object destructor */
Animal::~Animal()
{
    printf("Animal %s is being removed\n", name);
}

void Animal::show(void)
{
    printf("This animal is a %s, aged %d\n", name, age);
}


int main(int argc, char ** argv)
{
    Animal a(6,"dog");
    a.show();
}

