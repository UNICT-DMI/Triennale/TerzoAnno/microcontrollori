/*
 * point_path.cc
 */

#include <iostream>
#include <vector>
#include <math.h>

using namespace std;

class Point {
    float x, y;
public:
    Point();
    Point(float _x, float _y);
    float get_x() { return x; };
    float get_y() { return y; };
    float distance(Point &p);
};


Point::Point()
{
    x = y = 0;
}

Point::Point(float _x, float _y)
{
    x = _x;
    y = _y;
}

float Point::distance(Point &p)
{
    float dx = x - p.get_x();
    float dy = y - p.get_y();
    return sqrt(dx*dx + dy*dy);
}


ostream & operator<<(ostream & out, Point & p)
{
    out << "(" << p.get_x() << "," << p.get_y() << ")";
    return out;
}



class Path {
    vector<Point> points;
public:
    Path() {};
    Point & operator[](int index) { return points[index]; };
    int size() { return points.size(); };
    Path & operator+=(Point p);
    float length();
};

Path & Path::operator+=(Point p)
{
    points.push_back(p);
    return *this;
}

float Path::length()
{
    float dist = 0;
    for (int i = 0; i < points.size() - 1;i++) {
        Point p = points[i];
        dist += p.distance(points[i+1]);
    }
    return dist;
}

ostream & operator<<(ostream & out, Path & p)
{
    for (int i = 0; i < p.size();i++) {
        out << p[i] << ", ";
    }
    return out;
}

main()
{
    Path path;
    path += Point(0,0);
    path += Point(100,100);
    path += Point(100,150);

    cout << "Path is " << path << endl;
    cout << "Total path len = " << path.length() << endl;
}

