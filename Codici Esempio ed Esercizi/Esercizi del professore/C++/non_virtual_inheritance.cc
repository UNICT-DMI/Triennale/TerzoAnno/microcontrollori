/*
 * virtual_inheritance.cc
 */

#include <stdio.h>

class Animal {
protected:
    int age;
    char * name;
public:
    Animal(int a, char * n);
    ~Animal();
    void show(void);
};


class Dog : public Animal {
public:
    Dog(int a);
};


class Cat : public Animal {
public:
    Cat(int a);
    void show(void);
};


/* object constructor */
Animal::Animal(int a, char * n)
{
    age = a;
    name = n;
    printf("Animal %s is being created\n", name);
}

/* object destructor */
Animal::~Animal()
{
    printf("Animal %s is being removed\n", name);
}

/* The Dog constructor calls the Animal constructor */
Dog::Dog(int a) : Animal(a, "dog")
{
    printf("This is the constructor of 'Dog'\n");
}


/* The Cat constructor calls the Animal constructor */
Cat::Cat(int a) : Animal(a, "kitty")
{
    printf("This is the constructor of 'Cat'\n");
}




void Animal::show(void)
{
    printf("This animal is a %s, aged %d\n", name, age);
}


void Cat::show(void)
{
    printf(">>>>>>> This cat is called %s, aged %d\n", name, age);
}


int main(int argc, char ** argv)
{
    Cat * kitty = new Cat(3);
    kitty->show();
    Animal * a = kitty;
    a->show();
}
