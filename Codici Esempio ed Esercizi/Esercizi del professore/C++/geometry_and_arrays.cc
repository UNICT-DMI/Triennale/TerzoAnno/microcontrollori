/*
 * geometry.cc
 */

#include <iostream>

using namespace std;

class Polygon {
    int n_sides;
    float * sides;
public:
    Polygon() { n_sides = 0; };
    Polygon(int sd);
    ~Polygon();
    void set_side(int side_num, float side_val)
    {
        sides[side_num] = side_val;
    };
    float get_side(int side_num)
    {
        return sides[side_num];
    };
    virtual float perimeter();
    virtual float area() = 0;
};

class Rectangle : public Polygon {
public:
    Rectangle();
    Rectangle(float h, float w);
    virtual float area();
};


class Square : public Rectangle {
public:
    Square() { };
    Square(float s);
    void set_side(float side_val);
};


Polygon::Polygon(int sd)
{
    cout << "Creating the polygon..." << endl;
    this->n_sides = sd;
    this->sides = new float[sd];
}

Polygon::~Polygon()
{
    cout << "Destroying the polygon..." << endl;
    delete [] sides;
}

float Polygon::perimeter()
{
    float p = 0;
    for (int i = 0; i < n_sides;i++)
        p += sides[i];
    return p;
}

Rectangle::Rectangle() : Polygon(4) { };

Rectangle::Rectangle(float h, float w) : Polygon(4)
{
    set_side(0, h);
    set_side(1, w);
    set_side(2, h);
    set_side(3, w);
}

float Rectangle::area()
{
    return get_side(0) * get_side(1);
}

Square::Square(float s) : Rectangle(s, s)
{
}

void Square::set_side(float side_val)
{
    Polygon::set_side(0, side_val);
    Polygon::set_side(1, side_val);
    Polygon::set_side(2, side_val);
    Polygon::set_side(3, side_val);
}


main()
{
    Square * s = new Square[2];

    s[0].set_side(10);
    s[1].set_side(20);

    cout << "The perimeter of the first square is " << s[0].perimeter() << endl;
    cout << "The perimeter of the second square is " << s[1].perimeter() << endl;

    delete [] s;
}
