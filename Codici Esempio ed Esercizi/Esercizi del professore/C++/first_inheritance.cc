/*
 * first_inheritance.cc
 */

#include <stdio.h>

class Animal {
protected:
    int age;
    char * name;
public:
    Animal(int a, char * n);
    ~Animal();
    void show(void);
};


class Dog : public Animal {
public:
    Dog(int a);
};


class Cat : public Animal {
public:
    Cat(int a);
    void show(void);
};


/* object constructor */
Animal::Animal(int a, char * n)
{
    age = a;
    name = n;
    printf("Animal %s is being created\n", name);
}

/* object destructor */
Animal::~Animal()
{
    printf("Animal %s is being removed\n", name);
}

void Animal::show(void)
{
    printf("This animal is a %s, aged %d\n", name, age);
}


/* The Dog constructor calls the Animal constructor */
Dog::Dog(int a) : Animal(a, "dog")
{
    printf("This is the constructor of 'Dog'\n");
}


/* The Cat constructor calls the Animal constructor */
Cat::Cat(int a) : Animal(a, "cat")
{
    printf("This is the constructor of 'Cat'\n");
}


void Cat::show(void)
{
    printf("This a cat named '%s', aged %d\n", name, age);
}


void test_fun(Animal a)
{
    printf("We are in 'test_fun':");
    a.show();
}


int main(int argc, char ** argv)
{
    Dog argo(6);
    Cat kitty(3);

    argo.show();
    kitty.show();

    test_fun(kitty);
}

