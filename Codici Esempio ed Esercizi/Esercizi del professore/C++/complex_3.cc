/*
 * complex.cc
 */

#include <iostream>

class Complex {
    float re, im;
public:
    Complex();
    Complex(float r, float i);
    void set(float r, float i) { re = r; im = i; };
    void show();
    friend Complex operator+(Complex &a, Complex &b);
    friend Complex operator*(Complex &a, Complex &b);
    friend std::ostream &operator<<(std::ostream &out, Complex& c);
};

Complex::Complex()
{
    re = 0;
    im = 0;
}

Complex::Complex(float r, float i)
{
    re = r;
    im = i;
}

void Complex::show()
{
    std::cout << re << " + " << im << "i" << std::endl;
}


// friend functions

Complex operator+(Complex &a, Complex &b)
{
    Complex result;

    result.set(a.re + b.re, a.im + b.im);
    return result;
}

Complex operator*(Complex &a, Complex &b)
{
    Complex result;

    result.set(a.re * b.im - a.im * b.re, a.im * b.re + a.re * b.im);
    return result;
}

std::ostream &operator<<(std::ostream &out, Complex& c)
{
    out << c.re << " + " << c.im << "i";
    return out;
}



using namespace std;

main()
{
    Complex a(1,2), b(3,4);

    Complex c = a + b;
    Complex d = a * b;

    cout << a << "," << b << "," << c << "," << d << endl;
}


