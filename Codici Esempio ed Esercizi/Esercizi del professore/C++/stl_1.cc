/*
 * stl_1.cc
 */

#include <iostream>
#include <vector>

using namespace std;


main()
{
    vector<float> v;

    for (;;) {
        float data;
        cout << "Enter number (-1 to quit) ";
        cin >> data;
        if (data == -1) break;
        v.push_back(data);
    }

    for (int i = 0; i < v.size();i++)
        cout << v[i] << ", ";

    cout << endl;
}
