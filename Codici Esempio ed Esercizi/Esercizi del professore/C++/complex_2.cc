/*
 * complex.cc
 */

#include <iostream>

class Complex {
public:
    float re, im;
    Complex();
    Complex(float r, float i);
    void set(float r, float i) { re = r; im = i; };
    float real() { return re; };
    float imag() { return im; };
    void show();
};

Complex::Complex()
{
    re = 0;
    im = 0;
}

Complex::Complex(float r, float i)
{
    re = r;
    im = i;
}

void Complex::show()
{
    std::cout << re << " + " << im << "i" << std::endl;
}

Complex operator+(Complex &a, Complex &b)
{
    Complex result;

    result.set(a.re + b.re, a.im + b.im);
    return result;
}

Complex operator*(Complex& a, Complex &b)
{
    Complex result;

    result.set(a.re * b.im - a.im * b.re, a.im * b.re + a.re * b.im);
    return result;
}


main()
{
    Complex a(1,2), b(3,4);

    Complex c = a + b;
    Complex d = a * b;

    a.show();
    b.show();
    c.show();
    d.show();
}


