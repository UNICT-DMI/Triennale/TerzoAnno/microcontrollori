/*
 * complex.cc
 */

#include <iostream>

using namespace std;

class Complex {
    float re, im;
public:
    Complex();
    Complex(float r, float i);
    void set(float r, float i) { re = r; im = i; };
    float real() { return re; };
    float imag() { return im; };
    void show(bool newline = false);
    Complex operator+(Complex& p);
    Complex operator+(double p);
    Complex operator*(Complex &p);
    bool operator==(Complex& p);
};

Complex::Complex()
{
    re = 0;
    im = 0;
}

Complex::Complex(float r, float i)
{
    re = r;
    im = i;
}

void Complex::show(bool newline)
{
    cout << re << " + " << im << "i";
    if (newline)
        cout << std::endl;
}

Complex Complex::operator+(Complex & p)
{
    Complex result;

    result.set(re + p.re, im + p.im);
    return result;
}

Complex Complex::operator+(double d)
{
    Complex result;

    result.set(re + d, im);
    return result;
}

Complex Complex::operator*(Complex& p)
{
    Complex result;

    result.set(re * p.im - im * p.re, im * p.re + re * p.im);
    return result;
}


bool Complex::operator==(Complex& p)
{
    return (re == p.re && im == p.im);
}


main()
{
    Complex a(1,2), b(3,4);

    Complex c = a + b;
    Complex d = a * b;
    Complex f = a + 5.0;

    a.show(true);
    b.show(true);
    c.show(true);
    d.show(true);
    f.show(true);

    Complex e(1,2);

    if (a == e) {
        a.show(); cout << " and ";
        e.show(); cout << " are the same" << endl;
    }
    else {
        a.show(); cout << " and ";
        e.show(); cout << " are NOT the same" << endl;
    }

}


