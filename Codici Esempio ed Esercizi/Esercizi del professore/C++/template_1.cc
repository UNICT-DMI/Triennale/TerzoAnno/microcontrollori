/*
 * template_1.cc
 */

#include <iostream>

using namespace std;

template <class T> class Vector {
    T * data;
    int current_size, array_size;
    static const int space = 20;
public:
    Vector();
    ~Vector();
    int size() { return current_size; };
    Vector& operator+=(T item);
    T& operator[](int index);
};

template <class T>
Vector<T>::Vector()
{
    data = new T[space];
    current_size = 0;
    array_size = space;
}


template <class T>
Vector<T>::~Vector()
{
    delete [] data;
}


template <class T>
Vector<T>& Vector<T>::operator+=(T item)
{
    T * new_data;
    if (current_size < array_size) {
        data[current_size] = item;
        current_size++;
    }
    else {
        array_size += space;
        new_data = new T[array_size];
        for (int i = 0;i < current_size;i++)
            new_data[i] = data[i];
        new_data[current_size] = item;
        current_size++;
        delete [] data;
        data = new_data;
    }
    return *this;
}


template <class T>
T& Vector<T>::operator[](int index)
{
    return data[index];
}


template <class T> ostream& operator<<(ostream& out, Vector<T> & s)
{
    for (int i = 0; i < s.size();i++) {
        out << s[i] << " ";
    }
    return out;
}

main()
{
    Vector<float> f_Vector;
    Vector<int>   i_Vector;

    i_Vector += 4;
    i_Vector += 3;
    i_Vector += 2;
    i_Vector += 1;
    i_Vector += 0;

    f_Vector += 3.14;
    f_Vector += 2.71;

    cout << "INT VECTOR=" << i_Vector << endl;
    cout << "FLOAT VECTOR=" << f_Vector << endl << endl;

    i_Vector[3] = 10;

    cout << "INT VECTOR=" << i_Vector << endl;
}
