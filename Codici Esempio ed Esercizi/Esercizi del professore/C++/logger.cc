/*
 * logger.cc
 */

#include <iostream>
#include <fstream>
#include <string>

using namespace std;

class Logger {
    ofstream log_file;
public:
    Logger(const char * filename = "default.log");
    ~Logger();
    void log(char * s);
    void log(int i);
    void log(double f);
};


Logger::Logger(const char * filename)
{
    log_file.open(filename);
}

Logger::~Logger()
{
    log_file.close();
}

void Logger::log(char * s)
{
    log_file << "LOG_STRING: " << s << "\n";
}

void Logger::log(int i)
{
    log_file << "LOG_INT: " << i << "\n";
}

void Logger::log(double f)
{
    log_file << "LOG_DOUBLE: " << f << "\n";
}



main()
{
    Logger * std_logger = new Logger();
    Logger * my_logger = new Logger("mylogfile.txt");

    std_logger->log((char *)"Hello");
    std_logger->log(10);
    std_logger->log(20.3);

    my_logger->log((char *)"Ciao");
    my_logger->log(30);
    my_logger->log(40.3);

    delete std_logger;
    delete my_logger;
}


