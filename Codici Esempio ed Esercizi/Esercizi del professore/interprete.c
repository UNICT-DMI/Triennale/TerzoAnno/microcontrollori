#include <stdio.h>
#include <stdlib.h>
#include <string.h>

struct param {
	char name[30];
	int value;
};

int interpreter(char * in_string, struct param * p)
{
	int n;
	char * s;
	n = 0;
	s = strtok(in_string, "&");
	while (s != NULL) {
		char * eq_pos;
		eq_pos = strchr(s, '=');
		if (eq_pos != NULL) {
			strncpy(p[n].name, s, eq_pos - s);
			p[n].value = atoi(eq_pos + 1);
			n++;
		}
		s = strtok(NULL, "&");
	}
	return n;
}

void main()
{
	char str[200];
	struct param p[100];
	int n, i;
	strcpy(str, "p=10&name2=30&param4=-1&param3=20");
	n = interpreter(str, p);
	for (i = 0; i < n;i++) {
		printf("%s,%d\n", p[i].name, p[i].value);
	}
}

