/*
 * File:   serial_test.c
 * Author: corrado
 *
 * Created on December 11, 2012, 5:05 PM
 */


#define _XTAL_FREQ   8000000

#include <xc.h>
#include <pic16f690.h>
#include <delays.h>
#include <stdio.h>

// PIC16F690 Configuration Bit Settings

// CONFIG
#pragma config FOSC = INTRCIO   // Oscillator Selection bits (INTOSCIO oscillator: I/O function on RA4/OSC2/CLKOUT pin, I/O function on RA5/OSC1/CLKIN)
#pragma config WDTE = OFF       // Watchdog Timer Enable bit (WDT disabled and can be enabled by SWDTEN bit of the WDTCON register)
#pragma config PWRTE = OFF      // Power-up Timer Enable bit (PWRT disabled)
#pragma config MCLRE = ON       // MCLR Pin Function Select bit (MCLR pin function is MCLR)
#pragma config CP = OFF         // Code Protection bit (Program memory code protection is disabled)
#pragma config CPD = OFF        // Data Code Protection bit (Data memory code protection is disabled)
#pragma config BOREN = ON       // Brown-out Reset Selection bits (BOR enabled)
#pragma config IESO = ON        // Internal External Switchover bit (Internal External Switchover mode is enabled)
#pragma config FCMEN = ON       // Fail-Safe Clock Monitor Enabled bit (Fail-Safe Clock Monitor is enabled)

void putch(unsigned char byte)
{
    /* output one byte */
    TXREG = byte;
    while(!TXSTAbits.TRMT) ;
}

int main(void)
{
    int i = 0;

    OSCCONbits.IRCF = 0b111;  // 8 Mhz clock

    ANSEL = 0;   // no analog inputs
    ANSELH = 0;

    TRISBbits.TRISB7 = 0; // TX PIN set to output
    TRISBbits.TRISB5 = 1; // RX PIN set to input

    // setup UART transmitter
    TXSTAbits.TX9 = 0; // 8 bit data
    TXSTAbits.TXEN = 1; // enable transmitter
    TXSTAbits.BRGH = 1; // high speed transmission

    // setup UART receiver
    RCSTAbits.SPEN = 1; // enable serial port
    RCSTAbits.RX9 = 0; // 8 bit data
    RCSTAbits.CREN = 1; // enable receiver

    // baud rate generator control
    BAUDCTLbits.BRG16 = 1; // 16 bit baud rate generator

    // baud rate generator value
    SPBRGH = 0;
    SPBRG = 207;

    printf("Starting\n\r");
    
    for (;;) {
        char c;
        while (PIR1bits.RCIF == 0) ;
        c = RCREG;
        putch(c);
    }

    return 0;
}
