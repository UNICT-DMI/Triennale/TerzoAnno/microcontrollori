/* 
 * File:   tmr0_test_1.c
 * Author: corrado
 *
 * Created on October 18, 2015, 6:18 PM
 */

#include <xc.h>
#include <stdio.h>
#include <stdlib.h>

int main(int argc, char** argv) {
    TRISBbits.TRISB0 = 0; // output

    T0CONbits.TMR0ON = 0; // stop the timer
    T0CONbits.T08BIT = 0; // timer configured as 16-bit
    T0CONbits.T0CS = 0; // use system clock
    T0CONbits.PSA = 0; // use prescaler
    T0CONbits.T0PS = 0b111; // prescaler 1:256 ('0b' is a prefix for binary)
    TMR0 = 0; // clear timer value
    T0CONbits.TMR0ON = 1; // start the timer

    for (;;) {
        unsigned int t;
        t = TMR0;
        if (t >= 31250) { // equivalent of 500 ms
            TMR0 = 0;
            LATBbits.LATB0 = !LATBbits.LATB0;
        }
    }

    return (EXIT_SUCCESS);
}

