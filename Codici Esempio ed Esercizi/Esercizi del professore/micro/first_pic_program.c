/*
 * File:   main.c
 * Author: corrado
 *
 * Created on November 25, 2012, 9:49 PM
 */

#define _XTAL_FREQ   8000000

#include <xc.h>
#include <pic16f690.h>
#include <delays.h>


// PIC16F690 Configuration Bit Settings

// CONFIG
#pragma config FOSC = INTRCIO   // Oscillator Selection bits (INTOSCIO oscillator: I/O function on RA4/OSC2/CLKOUT pin, I/O function on RA5/OSC1/CLKIN)
#pragma config WDTE = OFF       // Watchdog Timer Enable bit (WDT disabled and can be enabled by SWDTEN bit of the WDTCON register)
#pragma config PWRTE = OFF      // Power-up Timer Enable bit (PWRT disabled)
#pragma config MCLRE = ON       // MCLR Pin Function Select bit (MCLR pin function is MCLR)
#pragma config CP = OFF         // Code Protection bit (Program memory code protection is disabled)
#pragma config CPD = OFF        // Data Code Protection bit (Data memory code protection is disabled)
#pragma config BOREN = OFF      // Brown-out Reset Selection bits (BOR disabled)
#pragma config IESO = OFF       // Internal External Switchover bit (Internal External Switchover mode is disabled)
#pragma config FCMEN = OFF      // Fail-Safe Clock Monitor Enabled bit (Fail-Safe Clock Monitor is disabled)


int main(void) {
    /* configure internal clock at 8 MHz */
    OSCCONbits.IRCF = 0b111;

    /* disable all analog inputs */
    ANSEL = 0;
    ANSELH = 0;

    /* setup RC0 as output */
    TRISCbits.TRISC0 = 0;
    TRISCbits.TRISC1 = 0;
    TRISCbits.TRISC2 = 0;
    TRISCbits.TRISC3 = 0;
    /* set RC0 as 0*/
    PORTC = 0xf;

    /* wait 1 second */
    __delay_ms(1000);

    for (;;) {
        /* turn LED on */
        PORTCbits.RC0 = 1;
        /* wait 0.5 secs */
        __delay_ms(500);

        /* turn LED off */
        PORTCbits.RC0 = 0;
        /* wait 0.5 secs */
        __delay_ms(500);
    }

    return 0;
}
