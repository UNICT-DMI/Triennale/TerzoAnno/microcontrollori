/*
 * File:   timer_test.c
 * Author: corrado
 *
 * Created on November 28, 2012, 5:28 PM
 */

#define _XTAL_FREQ   4000000

#include <xc.h>
#include <pic16f690.h>
#include <delays.h>

// PIC16F690 Configuration Bit Settings

// CONFIG
#pragma config FOSC = INTRCIO   // Oscillator Selection bits (INTOSCIO oscillator: I/O function on RA4/OSC2/CLKOUT pin, I/O function on RA5/OSC1/CLKIN)
#pragma config WDTE = OFF       // Watchdog Timer Enable bit (WDT disabled and can be enabled by SWDTEN bit of the WDTCON register)
#pragma config PWRTE = OFF      // Power-up Timer Enable bit (PWRT disabled)
#pragma config MCLRE = ON       // MCLR Pin Function Select bit (MCLR pin function is MCLR)
#pragma config CP = OFF         // Code Protection bit (Program memory code protection is disabled)
#pragma config CPD = OFF        // Data Code Protection bit (Data memory code protection is disabled)
#pragma config BOREN = ON       // Brown-out Reset Selection bits (BOR enabled)
#pragma config IESO = ON        // Internal External Switchover bit (Internal External Switchover mode is enabled)
#pragma config FCMEN = ON       // Fail-Safe Clock Monitor Enabled bit (Fail-Safe Clock Monitor is enabled)

int main(void) {

    OSCCONbits.IRCF = 0b110;

    ANSEL = 0;
    ANSELH = 0;

    TRISCbits.TRISC0 = 0;
    TRISCbits.TRISC1 = 0;
    TRISCbits.TRISC2 = 0;
    TRISCbits.TRISC3 = 0;

    PORTCbits.RC0 = 1;
    PORTCbits.RC1 = 0;
    PORTCbits.RC2 = 1;
    PORTCbits.RC3 = 0;

    OPTION_REGbits.T0CS = 0;
    OPTION_REGbits.PS = 0b111;
    OPTION_REGbits.PSA = 0;

    for (;;) {
        PORTCbits.RC3 = !PORTCbits.RC3;
        TMR0 = 256 - 160;
        while (INTCONbits.T0IF == 0) ;
        INTCONbits.T0IF = 0;
    }

    return 0;
}
