/* 
 * File:   my_main.c
 * Author: corrado
 *
 * Created on October 18, 2015, 6:18 PM
 */

#include <xc.h>
#include <stdio.h>
#include <stdlib.h>

/*
 * 
 */
#define IDLE    0
#define KEY_A   1
#define KEY_B   2
#define KEY_C   3

int main(int argc, char** argv) {

    long count = 0;
    int state = IDLE;

    TRISAbits.TRISA3 = 1;
    TRISAbits.TRISA2 = 1;
    TRISCbits.TRISC2 = 1;
    TRISBbits.TRISB0 = 0;
    TRISBbits.TRISB1 = 0;
    TRISBbits.TRISB2 = 0;
    TRISBbits.TRISB5 = 0;
    LATBbits.LATB0 = 1;
    LATBbits.LATB1 = 1;
    LATBbits.LATB2 = 1;
    LATBbits.LATB5 = 1;
    for (;;) { // loop forever

        switch (state) {
            case IDLE:
                // check events related to IDLE state and change state
                if (PORTAbits.RA3 == 0) {
                    LATBbits.LATB0 = !LATBbits.LATB0;
                    state = KEY_A;
                } else if (PORTAbits.RA2 == 0) {
                    LATBbits.LATB1 = !LATBbits.LATB1;
                    state = KEY_B;
                } else if (PORTCbits.RC2 == 0) {
                    LATBbits.LATB5 = !LATBbits.LATB5;
                    state = KEY_C;
                }
                break;
            case KEY_A:
                // check events related to KEY_A state and change state
                if (PORTAbits.RA3 == 1)
                    state = IDLE;
                break;
            case KEY_B:
                // check events related to KEY_B state and change state
                if (PORTAbits.RA2 == 1)
                    state = IDLE;
                break;
            case KEY_C:
                // check events related to KEY_C state and change state
                if (PORTCbits.RC2 == 1)
                    state = IDLE;
                break;
        }

        ++count;
        if (count == 100000) {
            LATBbits.LATB2 = !LATBbits.LATB2; // toggle the led
            count = 0;
        }
    }
    return (EXIT_SUCCESS);
}

