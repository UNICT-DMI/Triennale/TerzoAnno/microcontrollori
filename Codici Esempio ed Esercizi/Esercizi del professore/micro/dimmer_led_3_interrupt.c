/* 
 * File:   dimmer_led_3_interrupt.c
 * Author: corrado
 */

#include <xc.h>
#include <stdio.h>
#include <stdlib.h>

unsigned char TIME_ON[] =  { 5,   10,  20,  50, 100, 250};
unsigned char TIME_OFF[] = {250, 245, 235, 205, 155,   5};

#define OFF   0
#define ON    1

#define KEY_STATE_NONE    0
#define KEY_STATE_PRESSED 1

char state, duty, key_state;

void interrupt isr()
{
    if (PIR1bits.TMR2IF == 1) { // period match
        if (state == OFF) {
            LATBbits.LATB0 = 0; // all leds on
            PR2 = TIME_ON[duty]; // load period register
            state = ON;
        }
        else {
            LATBbits.LATB0 = 1;//LATB = 0xff; // all leds off
            LATBbits.LATB1 = 0;
            PR2 = TIME_OFF[duty]; // load period register
            state = OFF;
        }
        PIR1bits.TMR2IF = 0; // reset interrupt flag
    }
}

int main(int argc, char** argv) {

    TRISAbits.RA3 = 1;
    TRISAbits.RA2 = 1;

    TRISBbits.TRISB0 = 0; // output
    TRISBbits.TRISB1 = 0; // output
    TRISBbits.TRISB2 = 0; // output
    TRISBbits.TRISB3 = 0; // output
    TRISBbits.TRISB4 = 0; // output
    TRISBbits.TRISB5 = 0; // output

    duty = 0;
    key_state = KEY_STATE_NONE;

    T2CONbits.TMR2ON = 0; // stop the timer
    T2CONbits.T2CKPS = 0b10; // 1:16 prescaler
    T2CONbits.T2OUTPS = 0b0000; // no postscaler
    TMR2 = 0; // reset timer
    PR2 = TIME_OFF[duty]; // load period register
    PIR1bits.TMR2IF = 0; // reset interrupt flag
    state = OFF;

    INTCONbits.GIE = 1; // global enable interrupts
    INTCONbits.PEIE = 1; // peripheral enable interrupts
    RCONbits.IPEN = 0; // no priorities

    PIE1bits.TMR2IE = 1; // enable interrupt on TMR2

    T2CONbits.TMR2ON = 1; // start the timer

    LATB = 0xff; // all leds off

    for (;;) {
        // keyboard handling
        if (PORTAbits.RA2 == 0) { // key B pressed
            if (duty < 5)
                duty++;
            while (PORTAbits.RA2 == 0) {} // wait for release (busy-wait now admittable)
        }

        else if (PORTAbits.RA3 == 0) { // key A pressed
            if (duty > 0)
                duty--;
            while (PORTAbits.RA3 == 0) {} // wait for release (busy-wait now admittable)
        }
        
    }
    return (EXIT_SUCCESS);
}

