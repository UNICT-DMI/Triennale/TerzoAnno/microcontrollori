/* 
 * File:   my_main.c
 * Author: corrado
 *
 * Created on October 18, 2015, 6:18 PM
 */

#include <xc.h>
#include <stdio.h>
#include <stdlib.h>

/*
 * 
 */
#define CHECK_FOR_PRESS 4894
#define CHECK_FOR_RELEASE 421
int main(int argc, char** argv) {

    long count = 0;
    int state1 = CHECK_FOR_PRESS , state2 = CHECK_FOR_PRESS;

    TRISAbits.TRISA3 = 1;
    TRISAbits.TRISA2 = 1;
    TRISBbits.TRISB0 = 0;
    TRISBbits.TRISB1 = 0;
    TRISBbits.TRISB2 = 0;
    LATBbits.LATB0 = 1;
    LATBbits.LATB1 = 1;
    LATBbits.LATB2 = 1;
    for (;;) { // loop forever

        if (state1 == CHECK_FOR_PRESS && PORTAbits.RA3 == 0) {
            LATBbits.LATB0 = !LATBbits.LATB0; // toggle the led
            state1 = CHECK_FOR_RELEASE;
        }
        else if (state1 == CHECK_FOR_RELEASE && PORTAbits.RA3 == 1) {
            state1 = CHECK_FOR_PRESS;
        }


        if (state2 == CHECK_FOR_PRESS && PORTAbits.RA2 == 0) {
            LATBbits.LATB1 = !LATBbits.LATB1; // toggle the led
            state2 = CHECK_FOR_RELEASE;
        } else if (state2 == CHECK_FOR_RELEASE && PORTAbits.RA2 == 1) {
            state2 = CHECK_FOR_PRESS;
        }


        ++count;
        if (count == 100000) {
            LATBbits.LATB2 = !LATBbits.LATB2; // toggle the led
            count = 0;
        }
    }
    return (EXIT_SUCCESS);
}

