/* 
 * File:   dimmer_led_3_interrupt.c
 * Author: corrado
 */

#include <xc.h>
#include <stdio.h>
#include <stdlib.h>

unsigned char TIME_ON[] =  { 5,   10,  20,  50, 100, 250};
unsigned char TIME_OFF[] = {250, 245, 235, 205, 155,   5};

#define OFF   0
#define ON    1

#define FLASH_INTERVAL  -15625

char pwm_state, led_state, flashing_state, duty;

void interrupt isr()
{
    if (PIR1bits.TMR2IF == 1) { // period match
        if (led_state == ON) {
            if (pwm_state == OFF) {
                LATBbits.LATB0 = 0; // led on
                PR2 = TIME_ON[duty]; // load period register
                pwm_state = ON;
            }
            else {
                LATBbits.LATB0 = 1; // led off
                PR2 = TIME_OFF[duty]; // load period register
                pwm_state = OFF;
            }
        }
        else {
            LATBbits.LATB0 = 1; // led off
            PR2 = TIME_OFF[duty]; // load period register
            pwm_state = OFF;
        }
        PIR1bits.TMR2IF = 0; // reset interrupt flag
    }
    if (INTCONbits.T0IF == 1) {
        TMR0 = FLASH_INTERVAL;
        if (flashing_state == ON)
            led_state = !led_state;
        else
            led_state = ON;
        INTCONbits.T0IF = 0;
    }
}

int main(int argc, char** argv) {

    TRISAbits.RA3 = 1;
    TRISAbits.RA2 = 1;

    TRISBbits.TRISB0 = 0; // output
    TRISBbits.TRISB1 = 0; // output
    TRISBbits.TRISB2 = 0; // output
    TRISBbits.TRISB3 = 0; // output
    TRISBbits.TRISB4 = 0; // output
    TRISBbits.TRISB5 = 0; // output

    duty = 0;
    led_state = OFF;
    flashing_state = OFF;

    T2CONbits.TMR2ON = 0; // stop the timer
    T2CONbits.T2CKPS = 0b10; // 1:16 prescaler
    T2CONbits.T2OUTPS = 0b0000; // no postscaler
    TMR2 = 0; // reset timer
    PR2 = TIME_OFF[duty]; // load period register
    PIR1bits.TMR2IF = 0; // reset interrupt flag
    pwm_state = OFF;

    T0CONbits.TMR0ON = 0; // stop the timer
    T0CONbits.T08BIT = 0; // timer configured as 16-bit
    T0CONbits.T0CS = 0; // use system clock
    T0CONbits.PSA = 0; // use prescaler
    T0CONbits.T0PS = 0b111; // prescaler 1:256 ('0b' is a prefix for binary)
    TMR0 = FLASH_INTERVAL; // initial timer value
    INTCONbits.T0IF = 0; // clear the overflow bit initially


    INTCONbits.GIE = 1; // global enable interrupts
    INTCONbits.PEIE = 1; // peripheral enable interrupts
    RCONbits.IPEN = 0; // no priorities

    INTCONbits.T0IE = 1; // enable interrupt on TMR0
    PIE1bits.TMR2IE = 1; // enable interrupt on TMR2

    T0CONbits.TMR0ON = 1; // start the timer 0
    T2CONbits.TMR2ON = 1; // start the timer 2

    LATB = 0xff; // all leds off

    for (;;) {
        // keyboard handling
        if (PORTAbits.RA2 == 0) { // key B pressed
            if (duty < 5)
                duty++;
            while (PORTAbits.RA2 == 0) {} // wait for release (busy-wait now admittable)
        }

        else if (PORTAbits.RA3 == 0) { // key A pressed
            if (duty > 0)
                duty--;
            while (PORTAbits.RA3 == 0) {} // wait for release (busy-wait now admittable)
        }
        
        else if (PORTCbits.RC2 == 0) { // key C pressed
            flashing_state = !flashing_state;
            while (PORTCbits.RC2 == 0) {} // wait for release (busy-wait now admittable)
        }

    }
    return (EXIT_SUCCESS);
}

