/* 
 * File:   my_main.c
 * Author: corrado
 *
 * Created on October 18, 2015, 6:18 PM
 */

#include <xc.h>
#include <stdio.h>
#include <stdlib.h>

/*
 * 
 */
int main(int argc, char** argv) {

    long count = 0;

    TRISAbits.TRISA3 = 1;
    TRISAbits.TRISA2 = 1;
    TRISBbits.TRISB0 = 0;
    TRISBbits.TRISB1 = 0;
    TRISBbits.TRISB2 = 0;
    LATBbits.LATB0 = 1;
    LATBbits.LATB1 = 1;
    LATBbits.LATB2 = 1;
    for (;;) { // loop forever
        if (PORTAbits.RA3 == 0) {
            LATBbits.LATB0 = !LATBbits.LATB0; // toggle the led
            while (PORTAbits.RA3 == 0) {} // busy-wait for release
        }
        else if (PORTAbits.RA2 == 0) {
            LATBbits.LATB1 = !LATBbits.LATB1; // toggle the led
            while (PORTAbits.RA2 == 0) {} // busy-wait for release
        }
        ++count;
        if (count == 100000) {
            LATBbits.LATB2 = !LATBbits.LATB2; // toggle the led
            count = 0;
        }
    }
    return (EXIT_SUCCESS);
}

