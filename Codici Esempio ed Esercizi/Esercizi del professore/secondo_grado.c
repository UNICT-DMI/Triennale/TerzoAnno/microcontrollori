
#include <stdio.h>
#include <math.h>

void main()
{
   float a, b, c, delta, x1, x2;
   a = 2;
   b = -10;
   c= 4;
   delta = b*b - 4*a*c;
   if (delta < 0) {
     printf("DELTA < 0, soluzione impossibile nel campo reale\n");
   }
   else {
      x1 = (-b + sqrt(delta)) / (2*a);
      x2 = (-b - sqrt(delta)) / (2*a);
      printf("X1 = %f, X2 = %f\n", x1, x2);
   }
}



