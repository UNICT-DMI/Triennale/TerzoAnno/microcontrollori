#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int string_tokenizer(char * in_string, char sep, char ** words)
{
	int i, num_parole;
	i = 0;
	words[0] = in_string;
	num_parole = 1;
	while (in_string[i] != 0) {
		if (in_string[i] == sep) {
			in_string[i] = 0;
			words[num_parole] = in_string+i+1;
			num_parole++;
		}
		i++;
	}
	return num_parole;
}

void main()
{
	char s[100];
	char *parole[20];
	int n, i;
	strcpy(s, "uno due tre quattro");
	n = string_tokenizer(s, ' ', parole);
	for (i = 0;i < n;i++) {
		printf("%s\n", parole[i]);
	}
}

