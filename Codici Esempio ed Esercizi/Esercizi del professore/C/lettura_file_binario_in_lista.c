/*
 * lettura_file_binario_in_lista.c
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

typedef struct {
    char str[4];
    int num;
} t_data;

typedef struct t_list_element {
    t_data data;
    struct t_list_element * next;
} t_list_element;


// gets the head of the list, the data to insert and returns the new head of the list
t_list_element * insert_in_order(t_list_element * lista, t_data d)
{
    t_list_element * curr, * prev, *p;
    p = malloc(sizeof(t_list_element));
    p->data = d;
    if (lista == NULL) {
        // the list is empty
        p->next = NULL;
        return p;
    }
    prev = NULL;
    curr = lista;
    while (curr != NULL) {
        if (strcmp(curr->data.str, d.str) > 0) {
            // position found
            if (prev == NULL) {
                // first item in list
                p->next = lista;
                return p;
            }
            else {
                // item in the middle of the list
                p->next = curr;
                prev->next = p;
                return lista;
            }
        }
        prev = curr;
        curr = curr->next;
    }
    // item at the end of the list
    prev->next = p;
    p->next = NULL;
    return lista;
}


// gets the filename and returns the list
t_list_element * read_binary(char * filename)
{
    FILE * f;
    t_list_element * lista, *p;
    t_data d;
    f = fopen(filename, "rb");
    if (f == NULL)
        return NULL;
    lista = NULL;
    while (fread(&d, sizeof(t_data), 1, f) == 1) {
        p = malloc(sizeof(t_list_element));
        p->data = d;
        p->next = lista;
        lista = p;
    }
    fclose(f);
    return lista;
}


// gets the filename and returns the ORDERED list
t_list_element * read_binary_in_order(char * filename)
{
    FILE * f;
    t_list_element * lista, *p;
    t_data d;
    f = fopen(filename, "rb");
    if (f == NULL)
        return NULL;
    lista = NULL;
    while (fread(&d, sizeof(t_data), 1, f) == 1) {
        lista = insert_in_order(lista, d);
    }
    fclose(f);
    return lista;
}


void print_list(t_list_element * l)
{
    if (l != NULL) {
        printf("%s, %d\n", l->data.str, l->data.num);
        print_list(l->next);
    }
}


int main(int argc, char ** argv)
{
    t_list_element * lista;
    lista = read_binary("myfile.bin");
    print_list(lista);
    printf("--------------------\n");
    lista = read_binary_in_order("myfile.bin");
    print_list(lista);
}
