#include <stdio.h>
#include <math.h>

int secondo_grado(float a, float b, float c, float * p_x1, float * p_x2)
{
   float delta;
   delta = b*b - 4*a*c;
   if (delta < 0) {
     return 0;
   }
   else {
      *p_x1 = (-b + sqrt(delta)) / (2*a);
      *p_x2 = (-b - sqrt(delta)) / (2*a);
      return 1;
   }
}

void main()
{
    float a, b, c, x1, x2;
    int r;
/*
    printf("Coefficiente a=");scanf("%f", &a);
    printf("Coefficiente b=");scanf("%f", &b);
    printf("Coefficiente c=");scanf("%f", &c);
*/
    printf("Inserisci i coefficienti:");
    scanf("%f%f%f", &a, &b, &c);
    r = secondo_grado(a, b, c, &x1, &x2);
    if (r == 0)
       printf("Non ci sono soluzioni in R\n");
    else
       printf("Radici: x1 = %f, x2 = %f\n", x1, x2);
}

