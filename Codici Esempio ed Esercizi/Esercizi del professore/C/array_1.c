#include <stdio.h>

void print_array(int * v, int n)
{
   int i;
   for (i = 0;i < n;i++)
     printf("Elemento %d = %d\n", i,v[i]);
}

void incr_array(int * v, int n)
{
   int i;
   for (i = 0;i < n;i++)
      v[i]++;
}

void main()
{
   int a[10];
   int i, b;
   b = 200;
   for (i = 0;i < 10;i++)
     *(a+i) = i*10+4;
   print_array(a, 10);
   incr_array(a, 10);
   print_array(a, 10);
   print_array(&b,1);
}
