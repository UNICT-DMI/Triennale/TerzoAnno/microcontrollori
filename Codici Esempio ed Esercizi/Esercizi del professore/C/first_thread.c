#include <stdio.h>
#include <pthread.h>

int global;

void * proc1(void * x)
{
   int i;
   for (i = 0;i < 10;i++) {
      printf("PROC1: %d, %d\n", i, global);
      sleep(1);
   }
   return NULL;
}


int main(int argc, char ** argv)
{
   int i;
   pthread_t first_thread;
   pthread_create(&first_thread, NULL, proc1, NULL);
   for (i = 0;i < 5;i++) {
      printf("MAIN: %d\n", i);
      global = i * 3;
      sleep(1);
   }
   pthread_join(first_thread, NULL);
   return 0;
}

