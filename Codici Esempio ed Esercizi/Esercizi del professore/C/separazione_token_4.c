#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int string_tokenizer(char * in_string, char sep, char ** words)
{
	int num_parole;
	char * p;
	words[0] = in_string;
	num_parole = 1;
	while ( (p = strchr(in_string, sep)) != NULL) {
		*p = 0;
		words[num_parole] = p+1;
		num_parole++;
		in_string = p+1;
	}
	return num_parole;
}

void main()
{
	char s[100];
	char *parole[20];
	int n, i;
	strcpy(s, "uno due tre quattro");
	n = string_tokenizer(s, ' ', parole);
	for (i = 0;i < n;i++) {
		printf("%s\n", parole[i]);
	}
}

