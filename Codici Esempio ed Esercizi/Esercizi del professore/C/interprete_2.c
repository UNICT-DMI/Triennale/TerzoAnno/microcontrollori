#include <stdio.h>
#include <stdlib.h>
#include <string.h>

struct param {
	char *name;
	int value;
};



void main()
{
	char str[200];
	struct param p[100];
	int n, i;
	strcpy(str, "p=10&name2=30&param4=-1&param3=20");

	n = get_param(str, p);

	for (i = 0; i < n;i++) {
		printf("%s,%d\n", p[i].name, p[i].value);
	}
}

int get_param(char * instr, struct param * params){
	
	int i,m,arg=0;
	char * fc;
	struct param * prs = params;
	
	fc = instr;
	i = strlen(instr) - 1;
	instr += i;
	while(1){
	 	m = 1;
		prs->value = 0;
		while(*instr != '='){
			if(*instr == '-')
				prs -> value*=-1;
			else{
				prs -> value += (*instr - '0') * m;
				m*=10;
			}
			instr--; 
		}
		*(instr--) = 0;
		while((*instr != '&') && (instr != fc))
			instr--;
		if(*instr == '&'){
			prs -> name = instr+1;
			prs++;
			arg++;
			instr--;
		}
		else {
			prs -> name=instr;
			return arg+1;
		}
	}
}

