#include <stdio.h>

int lungh(char * p)
{
	int n = 0;
	while (*p != 0) {
		p++;
		n++;
	}
	return n;
}

void main()
{
	char * s;
	int i;
	s = "Hello";
	printf("%s\n", s);
	for (i = 0; i < lungh(s);i++) {
		printf("%c, %d, %x\n", s[i], s[i], s[i]);
	}
}

