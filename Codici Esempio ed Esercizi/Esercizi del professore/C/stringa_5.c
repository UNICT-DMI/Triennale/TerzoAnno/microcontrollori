#include <stdio.h>
#include <string.h>

void main()
{
	char s[30];
	int i;
	strcpy(s, "Hello");
	printf("%s\n", s);
	for (i = 0; i < strlen(s);i++) {
		printf("%c, %d, %x\n", s[i], s[i], s[i]);
	}
	s[2] = 0;
	printf("%s\n", s);
}

