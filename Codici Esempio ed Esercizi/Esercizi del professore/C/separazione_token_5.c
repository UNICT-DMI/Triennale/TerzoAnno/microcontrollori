#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int string_tokenizer(char * in_string, char sep, char ** words)
{
	int num_parole;
	char * p;
	char s_sep[2];

	s_sep[0] = sep;
	s_sep[1] = 0;

	words[0] = strtok(in_string,s_sep);
	num_parole = 1;

	while ( (p = strtok(NULL, s_sep)) != NULL) {
		words[num_parole] = p;
		num_parole++;
	}
	return num_parole;
}

void main()
{
	char s[100];
	char *parole[20];
	int n, i;
	strcpy(s, "   uno     due  tre   quattro   ");
	n = string_tokenizer(s, ' ', parole);
	for (i = 0;i < n;i++) {
		printf("%s\n", parole[i]);
	}
}

