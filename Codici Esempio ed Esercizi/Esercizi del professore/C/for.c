#include <stdio.h>

void main()
{
    int i;
    for (i = 0; i < 10; i++) {
      printf("%d\n", i * 2);
    }
    i = 10;
    while (i >= 0) {
      float c;
      c = 1.0/i;
      printf("%f\n", c);
      i--;
    }
}

