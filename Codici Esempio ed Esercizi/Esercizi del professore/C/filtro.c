#include <stdio.h>
#include <stdlib.h>
#include <math.h>

float * filtro(float * in_set, int in_set_size, int * p_out_set_size)
{
   float * out_set;
   int i, j, n;
   n = 0;
   for (i = 0;i < in_set_size;i++) {
      if (fabs(in_set[i]) < 1) n++;
   }
   out_set = malloc(n * sizeof(float));
   for (i = 0, j = 0; i < in_set_size;i++) {
      if (fabs(in_set[i]) < 1) {
        out_set[j] = in_set[i];
        j++;
      }
   }
   *p_out_set_size = n;
   return out_set;
}

void main()
{
    float a[10] = {1.2, 0.5, 0.3, 10, 0.01, -3.0, -0.6, -1.2, 0, -0.7};
    float * b;
    int n,i;
    b = filtro(a, 10, &n);
    for (i = 0;i < n;i++)
      printf("%d = %f\n", i, b[i]);
}

