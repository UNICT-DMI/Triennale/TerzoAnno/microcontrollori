#include <stdio.h>
#include <stdlib.h>
#include <string.h>

void main()
{
	char * s;
	s = malloc(30*sizeof(char));
	printf("Inserisci una stringa:");
	scanf("%s", s);
	printf("%s\n", s);
	if (strcmp(s,"hello") == 0) {
		printf("uguali!!!!\n");
	}
	if (strcasecmp(s,"hello") == 0) {
		printf("uguali a meno del 'case'!!!!\n");
	}
	if (strncmp(s,"hello", 5) == 0) {
		printf("primi 5 caratteri uguali!!!!\n");
	}
	if (strncmp(s+2,"hello", 5) == 0) {
		printf("'hello' presente dal terzo carattere!!!!\n");
	}
}

