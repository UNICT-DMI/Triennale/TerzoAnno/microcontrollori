/*
 * eserc_21_10.c
 */

#include <stdio.h>
#include <stdlib.h>

int menu(void)
{
    int choice;
    printf("------------------------------------------------------\n");
    printf("0: fine\n");
    printf("1: genera dati\n");
    printf("2: stampa dati\n");
    printf("3: calcola min e max\n");
    printf("4: genera dati filtrati\n");
    printf("5: genera istogramma\n");
    printf("Scelta:");
    scanf("%d", &choice);
    return choice;
}

int generate_data(float * x)
{
    int i, n;
    printf("Inserisci dimensioni ( < 100):");
    scanf("%d", &n);

    srand(time(NULL));

    for (i = 0;i < n;i++)
       	x[i] = rand() * 100.0 / RAND_MAX;
    return n;
}

void print_data(float * x, int size)
{
    int i;
    for (i = 0;i < size;i++)
        printf("Index %d --> %f\n", i, x[i]);
}

void print_data_v2(float * x, int size)
{
    int i;
    for (i = 0;i < size;i++) {
        printf("Index %d --> %f\n", i, x[0]);
        x++;
    }
}

void min_max(float * x, int size)
{
    int i;
    float M = x[0], m = x[0];

    for (i = 1; i < size;i++) {
        if (x[i] > M) M = x[i];
        if (x[i] < m) m = x[i];
    }

    printf("MAX = %f, min = %f\n", M, m);
}

void filtered_data(float * x, int size)
{
    float threshold, filtered[100];
    int filtered_size, i;

    printf("Inserisci Soglia:");
    scanf("%f", &threshold);

    filtered_size = 0;

    for (i = 0; i < size;i++) {
        if (x[i] < threshold) {
            filtered[filtered_size] = x[i];
            filtered_size++;
        }
    }

    for (i = 0;i < filtered_size;i++)
        printf("Filtered %d --> %f\n", i, filtered[i]);

}

void histogram(float * x, int size)
{
    int i,occurrence[10];

    for (i = 0; i < 10;i++) {
        occurrence[i] = 0;
    }

    for (i = 0;i < size;i++) {
        int hist_index = (int)(x[i] / 10);
        occurrence[hist_index]++;
    }

    for (i = 0;i < 10;i++)
        printf("Range [%d,%d[ = %d occorrenze\n", i*10, i*10+10, occurrence[i]);

}

void main(void)
{
    float data[100];
    int data_size;

    for (;;) {
        switch (menu()) {
        case 0:
            return;
        case 1:
            data_size = generate_data(data);
            break;
        case 2:
            print_data(data, data_size);
            break;
        case 21:
            print_data_v2(data, data_size);
            break;
        case 3:
            min_max(data, data_size);
            break;
        case 4:
            filtered_data(data, data_size);
            break;
        case 5:
            histogram(data, data_size);
            break;
        }
    }
}

