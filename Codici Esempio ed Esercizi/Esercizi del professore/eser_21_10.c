/*
 * eserc_21_10.c
 */

#include <stdio.h>
#include <stdlib.h>

float data[100];
int data_size;

int menu(void)
{
    int choice;
    printf("------------------------------------------------------\n");
    printf("0: fine\n");
    printf("1: genera dati\n");
    printf("2: stampa dati\n");
    printf("3: calcola min e max\n");
    printf("4: genera dati filtrati\n");
    printf("5: genera istogramma\n");
    printf("Scelta:");
    scanf("%d", &choice);
    return choice;
}

void generate_data(void)
{
    int i;
    printf("Inserisci dimensioni ( < 100):");
    scanf("%d", &data_size);

    srand(time(NULL));

    for (i = 0;i < data_size;i++)
        data[i] = rand() * 100.0 / RAND_MAX;
}

void print_data(void)
{
    int i;
    for (i = 0;i < data_size;i++)
        printf("Index %d --> %f\n", i, data[i]);
}

void min_max(void)
{
    int i;
    float M = data[0], m = data[0];

    for (i = 1; i < data_size;i++) {
        if (data[i] > M) M = data[i];
        if (data[i] < m) m = data[i];
    }

    printf("MAX = %f, min = %f\n", M, m);
}

void filtered_data(void)
{
    float threshold, filtered[100];
    int filtered_size, i;

    printf("Inserisci Soglia:");
    scanf("%f", &threshold);

    filtered_size = 0;

    for (i = 0; i < data_size;i++) {
        if (data[i] < threshold) {
            filtered[filtered_size] = data[i];
            filtered_size++;
        }
    }

    for (i = 0;i < filtered_size;i++)
        printf("Filtered %d --> %f\n", i, filtered[i]);

}

void histogram(void)
{
    int i,occurrence[10];

    for (i = 0; i < 10;i++) {
        occurrence[i] = 0;
    }

    for (i = 0;i < data_size;i++) {
        int hist_index = (int)(data[i] / 10);
        occurrence[hist_index]++;
    }

    for (i = 0;i < 10;i++)
        printf("Range [%d,%d[ = %d occorrenze\n", i*10, i*10+10, occurrence[i]);

}

void main(void)
{
    for (;;) {
        switch (menu()) {
        case 0:
            return;
        case 1:
            generate_data();
            break;
        case 2:
            print_data();
            break;
        case 3:
            min_max();
            break;
        case 4:
            filtered_data();
            break;
        case 5:
            histogram();
            break;
        }
    }
}

