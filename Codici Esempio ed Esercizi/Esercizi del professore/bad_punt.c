#include <stdio.h>

int * f(void)
{
   int a[10]={1,2,3,4};
   int i;
   printf("Vettore all'interno di 'f'\n");
   for (i = 0;i < 10;i++)
     printf("%d=%d\n",i,a[i]);
   return a;
}

void main()
{
   int * v;
   int i;
   v = f();
   printf("Vettore all'esterno di 'f'\n");
   for (i = 0;i < 10;i++)
     printf("%d=%d\n",i, v[i]);
}
