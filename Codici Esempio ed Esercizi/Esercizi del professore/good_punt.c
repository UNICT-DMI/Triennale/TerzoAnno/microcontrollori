#include <stdio.h>
#include <stdlib.h>

int * f(void)
{
   int * a;
   int i;
   a = malloc(5*sizeof(int));
   a[0] = 1;
   a[1] = 2;
   a[2] = 3;
   a[3] = 4;
   a[4] = 5;
   printf("Vettore all'interno di 'f'\n");
   for (i = 0;i < 5;i++)
     printf("%d=%d\n",i,a[i]);
   return a;
}

void main()
{
   int * v;
   int i;
   v = f();
   printf("Vettore all'esterno di 'f'\n");
   for (i = 0;i < 5;i++)
     printf("%d=%d\n",i, v[i]);
}
