#include <stdio.h>
#include <pthread.h>

int global[20];
pthread_mutex_t my_mutex;
pthread_cond_t sveglia;

void * thread1(void * x)
{
   int i;
   for (;;) {
     pthread_mutex_lock(&my_mutex);
     /// attendi il thread 2
     pthread_cond_wait(&sveglia, &my_mutex);

     printf("THREAD1: ");
     for (i = 0;i < 20;i++) {
        printf("%d, ", global[i]);
     }
     printf("\n");
     pthread_mutex_unlock(&my_mutex);
   }
   return NULL;
}

void * thread2(void * x)
{
   int i, k;
   k = 0;
   for (;;) {
     pthread_mutex_lock(&my_mutex);
     for (i = 0;i < 20;i++) {
        global[i] = k;
     }
     /// sveglia il thread 1
     pthread_cond_signal(&sveglia);
     pthread_mutex_unlock(&my_mutex);
     k = (k + 1) % 100;
     sleep(1);
   }
   return NULL;
}


int main(int argc, char ** argv)
{
   int i;
   pthread_t thread_one,thread_two;

   pthread_mutex_init(&my_mutex, NULL);
   pthread_cond_init(&sveglia, NULL);

   pthread_create(&thread_one, NULL, thread1, NULL);
   pthread_create(&thread_two, NULL, thread2, NULL);
   pthread_join(thread_one, NULL);
   return 0;
}

