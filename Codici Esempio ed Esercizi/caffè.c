/* 
 * Coded by Helias, Squalex 
 * Macchinetta del caff�
 */

#include <xc.h>

void main(void) {
    /* buttons */
    TRISAbits.TRISA3 = 1; // caff�
    TRISAbits.TRISA2 = 1; // caff� macchiato
    TRISCbits.TRISC2 = 1; // selezione zucchero
    
    /* LED erogazione*/
    TRISBbits.TRISB0 = 0; // caff�
    TRISBbits.TRISB1 = 0; // latte (caff� macchiato)
    TRISBbits.TRISB2 = 0; // zucchero
    
    /* LED zucchero */
    TRISBbits.TRISB3 = 0;
    TRISBbits.TRISB4 = 0;
    TRISBbits.TRISB5 = 0;

    int zucchero = 0;
    int stato = 0;
    
    for(;;) {

        if (PORTCbits.RC2 == 0) {
            /* se il pulsante RC2 viene premuto
             * incrementiamo lo zucchero
             * ed accendiamo i relativi LED
             */
            zucchero = (zucchero + 1) % 4;
            
            switch(zucchero) {
                case 0:
                    LATBbits.LATB3 = 1;
                    LATBbits.LATB4 = 1;
                    LATBbits.LATB5 = 1;
                  break;
                case 1: LATBbits.LATB3 = 0; break;
                case 2: LATBbits.LATB4 = 0; break;
                case 3: LATBbits.LATB5 = 0; break;
            }
            
            while (PORTCbits.RC2 == 0) {} // busy-wait
        }
        
        
        if (PORTAbits.RA3 == 0 || PORTAbits.RA2 == 0) {

            if (PORTAbits.RA3 == 0)
                stato = 1;
            else
               stato = 2;
            
            while (PORTAbits.RA3 == 0 || PORTAbits.RA2 == 0) {} // busy-wait                
        }
        
        if (stato != 0) {

            /* erogazione zucchero */
            for (long i = 0; i < zucchero*45000; i++)
                LATBbits.LATB2 = 0;

            LATBbits.LATB2 = 1;

           /* erogazione latte (caff� macchiato) */ 
            if (stato == 2)
                for (long i = 0; i < 200000; i++)
                    LATBbits.LATB1 = 0;

            LATBbits.LATB1 = 1;            
            
            /* erogazione caff� */
            for (long i = 0; i < 200000; i++)
                LATBbits.LATB0 = 0;
            
            LATBbits.LATB0 = 1;

            stato = 0;
        }
        
    }
    
}

