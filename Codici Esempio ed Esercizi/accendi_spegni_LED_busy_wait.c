/* Coded by Helias */

#include <xc.h>

void main(void) {

    TRISAbits.TRISA3 = 1; /* button */

	TRISBbits.TRISB0 = 0; /* LED */
    
	for (;;) {
        if (PORTAbits.RA3 == 0) {
           LATBbits.LATB0 = !LATBbits.LATB0;

            while (PORTAbits.RA3 == 0) {} // busy-wait                
        }
	}

}
