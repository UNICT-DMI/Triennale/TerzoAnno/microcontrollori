#include <xc.h>
#include <stdio.h>

int min = 0;
int sec = 0;
int cent = 0;

int state = 0;

int count = 0;
char arr[10][10] = {};

void setup_timer() {
	T0CONbits.TMR0ON = 0; 	// stop the timer
	T0CONbits.T08BIT = 0; 	// timer configured as 16-bit
	T0CONbits.T0CS = 0; 	// use system clock
	T0CONbits.PSA = 0; 		// use prescaler
	T0CONbits.T0PS = 0b111; // prescaler 1:256 ('0b' is a prefix for binary)
	TMR0 = 0; 				// clear timer value
	T0CONbits.TMR0ON = 1; 	// start the timer
}

void setup_adc(void)
{
    ANSELAbits.ANSA0 = 1; // RA0 = analog input --> AN0
    ANSELAbits.ANSA1 = 1; // RA1 = analog input --> AN1
    ADCON1bits.PVCFG = 0b00; // Positive reference = +VDD
    ADCON1bits.NVCFG = 0b00; // Negative reference = GND
    ADCON2bits.ADFM = 1; // format = right justified
    ADCON2bits.ACQT = 0b111; // acquisition time = 20*TAD
    ADCON2bits.ADCS = 0b110; // conversion clock = FOSC/64
    ADCON0bits.ADON = 1; // turn on ADC    
}

void setup_uart(void)
{
    // setup UART
    TRISCbits.TRISC6 = 0; // TX as output
    TRISCbits.TRISC7 = 1; // RX as input
    TXSTA1bits.SYNC = 0; // Async operation
    TXSTA1bits.TX9 = 0; // No tx of 9th bit
    TXSTA1bits.TXEN = 1; // Enable transmitter
    RCSTA1bits.RX9 = 0; // No rx of 9th bit
    RCSTA1bits.CREN = 1; // Enable receiver
    RCSTA1bits.SPEN = 1; // Enable serial port
    // Setting for 19200 BPS
    BAUDCON1bits.BRG16 = 0; // Divisor at 8 bit
    TXSTA1bits.BRGH = 0; // No high-speed baudrate
    SPBRG1 = 51; // divisor value for 19200
}

void putch(char c)
{
    while (TXSTA1bits.TRMT == 0) {}
    TXREG1 = c;
}

int interrupt_() {

	if (INTCONbits.T0IF == 1) {
		TMR0 = -625; // 10ms
		INTCONbits.T0IF = 0;
		cent++;

		if (cent == 100) {
			cent = 0;
			sec++;

			if (sec == 60) {
				sec = 0;
				min++;
			}
		}

		return 1;
	}

	return 0;
}

void setup_buttons_leds() {

	// buttons
	TRISAbits.TRISA3 = 1; // RUN/STOP
	TRISAbits.TRISA2 = 1; // RESET/MEM7SET-UP
	TRISCbits.TRISC2 = 1; // SHOW-MEM

	// LEDs
	TRISBbits.TRISB0 = 0; // cronometro attivo
	TRISBbits.TRISB2 = 0; // indicatore timer
}

void run() {
	if (interrupt_())
		LATBbits.LATB2 = !LATBbits.LATB2;

	LATBbits.LATB0 = 0;

	printf("%2d:%2d:%d \r\n", min, sec, cent);
}

void stop() {
	LATBbits.LATB2 = 1;
	LATBbits.LATB0 = 1;
}

void run_stop() {
	if (PORTAbits.RA3 == 0) {

		if (state == 0)
			state = 1; // stop
		else
			state = 0; // run
	}
	while (PORTAbits.RA3 == 0) {}
}

void azzera() {
	int i = 0;
	for (i = 0; i < 10; i++)
		sprintf(arr[i], "");

	count = 0;
}

void reset_mem_setup() {

	// SET-UP after 2 secs
	if (state == 1) {

		if (PORTAbits.RA2 == 0) {
			cent = 0;
			sec = 0;
			min = 0;

			azzera();
		}

		while (PORTAbits.RA2 == 0) {
			interrupt_();
	
			if (sec >= 2) {
				cent = 0;
				sec = 0;
				min = 0;

				state = 2; // set-up
			}
		}

	}

	if (state == 2) { // if SET-UP

		// AN0
		ADCON0bits.CHS = 0;
		ADCON0bits.GODONE = 1;
		while (ADCON0bits.GODONE == 1) {}

		min = (60*ADRES)/1024;

		// AN1
		ADCON0bits.CHS = 1;
		ADCON0bits.GODONE = 1;
		while (ADCON0bits.GODONE == 1) {}

		sec = (60*ADRES)/1024;

		printf("min: %d, sec: %d \n\r", min, sec);

		if (PORTAbits.RA2 == 0)
			state = 1;

		while (PORTAbits.RA2 == 0) {}
	}

	if (state == 0) { // if RUN

		if (PORTAbits.RA2 == 0 && count < 10) {
			sprintf(arr[count], "%d:%d:%d", min, sec, cent);
			count++;
		}
		while(PORTAbits.RA2 == 0) {}

		/* NO BUSY-WAIT
		if (PORTAbits.RA2 == 0 && count < 10) {
			sprintf(arr[count], "%d:%d:%d", min, sec, cent);
			count++;
			press = 1;
		}
		else if (PORTAbits.RA2 == 0 && press == 1) {
			press = 0;

		*/
	}

}

void show_mem() {
	if (state == 1) { // if STOP

		if (PORTCbits.RC2 == 0) {

			int i = 0;
			for (i = 0; i < 10; i++)
				printf("%s \n\r", arr[i]);

		}
		while (PORTCbits.RC2 == 0) {}

	}
}

void config() {
	switch(state) {
		case 0: run(); break;
		case 1: stop(); break;
	}
}

void main(void)
{
		setup_timer();
    setup_adc();
    setup_uart();
		setup_buttons_leds();

    for (;;) {

			config();
			run_stop();
			reset_mem_setup();
			show_mem();

    }

    return;
}

