/* Coded By Helias (Stefano Borzì) */

#include <xc.h>
#include <stdio.h>

#define TG 3
int TR = 8;
int TS = 5;

const char *nomi_stato[] = { "VERDE", "GIALLO", "ROSSO", "ATTESA" };
int time = 0;

void setup_timer() {
	T0CONbits.TMR0ON = 0; 	// stop the timer
	T0CONbits.T08BIT = 0; 	// timer configured as 16-bit
	T0CONbits.T0CS = 0; 	// use system clock
	T0CONbits.PSA = 0; 		// use prescaler
	T0CONbits.T0PS = 0b111; // prescaler 1:256 ('0b' is a prefix for binary)
	TMR0 = 0; 				// clear timer value
	T0CONbits.TMR0ON = 1; 	// start the timer
}

void setup_adc(void)
{
    ANSELAbits.ANSA0 = 1; // RA0 = analog input --> AN0
    ANSELAbits.ANSA1 = 1; // RA1 = analog input --> AN1
    ADCON1bits.PVCFG = 0b00; // Positive reference = +VDD
    ADCON1bits.NVCFG = 0b00; // Negative reference = GND
    ADCON2bits.ADFM = 1; // format = right justified
    ADCON2bits.ACQT = 0b111; // acquisition time = 20*TAD
    ADCON2bits.ADCS = 0b110; // conversion clock = FOSC/64
    ADCON0bits.ADON = 1; // turn on ADC    
}

void setup_uart(void)
{
    // setup UART
    TRISCbits.TRISC6 = 0; // TX as output
    TRISCbits.TRISC7 = 1; // RX as input
    TXSTA1bits.SYNC = 0; // Async operation
    TXSTA1bits.TX9 = 0; // No tx of 9th bit
    TXSTA1bits.TXEN = 1; // Enable transmitter
    RCSTA1bits.RX9 = 0; // No rx of 9th bit
    RCSTA1bits.CREN = 1; // Enable receiver
    RCSTA1bits.SPEN = 1; // Enable serial port
    // Setting for 19200 BPS
    BAUDCON1bits.BRG16 = 0; // Divisor at 8 bit
    TXSTA1bits.BRGH = 0; // No high-speed baudrate
    SPBRG1 = 51; // divisor value for 19200
}

void putch(char c)
{
    while (TXSTA1bits.TRMT == 0) {}
    TXREG1 = c;
}

void setup_buttons_leds() {

	// buttons
	TRISAbits.TRISA3 = 1; // A
	TRISAbits.TRISA2 = 1; // B
	TRISCbits.TRISC2 = 1; // config

	// LEDs
	TRISBbits.TRISB0 = 0; // "Rosso"
	TRISBbits.TRISB2 = 0; // "Gialla"
	TRISBbits.TRISB4 = 0; // "Verde"

	TRISBbits.TRISB5 = 0; // config


	LATBbits.LATB4 = 0;
}

int interrupt_() {
	if (INTCONbits.T0IF == 1) {
		TMR0 = -62500;
		INTCONbits.T0IF = 0;
		time++;
		return 1;
	}

	return 0;
}

void main(void)
{
	setup_buttons_leds();
	setup_timer();
	setup_uart();
	setup_adc();

	int state = 0; // Verde
	int stack = 0;

	int press = 0;

	for (;;) {

		if (state != 4) {
			printf("stato=%-6s, tempo=%2d\r", nomi_stato[state], time);
			LATBbits.LATB5 = 1;
		}
		else {
			LATBbits.LATB5 = 0;
			LATBbits.LATB4 = 1;

			// lettura AN0
			ADCON0bits.CHS = 0;
			ADCON0bits.GODONE = 1;
			while (ADCON0bits.GODONE == 1) {}

			TR = 8 + ((21-8)* ADRES) / 1024;

			// lettura AN1
      ADCON0bits.CHS = 1;
      ADCON0bits.GODONE = 1;
      while (ADCON0bits.GODONE == 1) {}

			TS = (ADRES * 16) / 1024;

			printf("TR: %d, TS: %d \r\n", TR, TS);
		}

		if (PORTCbits.RC2 == 0 && (state == 0 || state == 4) && stack == 0) {
			if (state == 4)
				state = 0;
			else
				state = 4;

			while (PORTCbits.RC2 == 0) {};
		}


		if (press == 0 && ((PORTAbits.RA2 == 0 || PORTAbits.RA3 == 0) || stack == 1) && state == 0) {
			press = 1;
			stack = 0;

			LATBbits.LATB2 = !LATBbits.LATB2;
			time  = 0;
			state = 1; // Giallo
		}
		else if (press == 1 && (PORTAbits.RA2 == 1 && PORTAbits.RA2 == 1)) {
			press = 0;
		}


		if (state == 0) { // Verde
			interrupt_();
			LATBbits.LATB4 = 0;
		}
		else if (state == 1) { // Giallo

			LATBbits.LATB2 = 0; // on Giallo
			LATBbits.LATB4 = 1; // off Verde

			if (interrupt_()) {
				if (time == TG) {
					time = 0;
					state = 2; // Rosso
				}
			}
		}
		else if (state == 2) { // Rosso
			LATBbits.LATB2 = 1; // off Giallo
			LATBbits.LATB0 = 0; // on Rosso

			if (interrupt_()) {
				if (time == TR) {
					LATBbits.LATB0 = 1; // off Rosso
					LATBbits.LATB4 = 0; // on Verde

					time = 0;
					state = 3;
				}
			}
		}
		else if (state == 3) { // wait
			if (PORTAbits.RA2 == 0 || PORTAbits.RA3 == 0)
				stack = 1;

			if (interrupt_()) {
				if (time == TS) {
					state = 0;
					time = 0;
				}
			}
		}

	}
}
